<!DOCTYPE HTML>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="1; url=http://example.com">
    <meta name="google-site-verification" content="rYt3OTx5i-oG1fEMTQ6O1r0Tz5HM9I3wmQOdjX9gixs" />
    <script type="text/javascript">
    window.location.href = "https://youtube.com/mrguinas"
    </script>
    <title>Page Redirection</title>
</head>

<body>
    <!-- Note: don't tell people to `click` the link, just tell them that it is a link. -->
    If you are not redirected automatically, follow this <a href='http://example.com'>link to example</a>.
</body>

</html>