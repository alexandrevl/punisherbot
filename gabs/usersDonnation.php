<?php
if(isset($_GET['v'])) {
        $v = $_GET['v'];
    }
$isColor = true;
if(isset($_GET['c'])) {
        $isColor = false;
    } 
?>
<html>

<head>
    <title>
        MrGuinas - Users Donnation
    </title>
    <link rel="icon" type="image/png" href="img/Logo.png" />
    <style>
    .foo {
        float: left;
        width: 20px;
        height: 20px;
        margin: 5px;
        border: 1px solid rgba(0, 0, 0, .2);
    }

    .blue {
        background: #0000FF;
    }

    .purple {
        background: #800080;
    }

    .red {
        background: #FF0000;
    }

    .green {
        background: #008000;
    }

    .orange {
        background: #FFA500;
    }

    .pink {
        background: #FFC0CB;
    }
    </style>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js">
    </script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js">
    </script>
    <script src="js/usersDonnation.js?v5" type="text/javascript"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
        integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid" id="container">
    </div>
</body>

</html>