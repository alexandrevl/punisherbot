<html>

<head>
    <title>Alert Box Widget - </title>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="icon" type="image/png" href="../img/Logo.png" />
    <script src="../js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js'></script>
    <script src="js/changeMeta.js?v=<?php echo uniqid(); ?>"></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet">
    <script src='https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js' type='text/javascript'></script>
    <script src="js/streamlabs.js?v=<?php echo uniqid(); ?>"></script>
</head>

<body>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="alert alert-danger" style="display: none" role="alert" id="alert">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form>
                    <div class="form-group row">
                        <label for="textMeta" class="col-sm-2 col-form-label">Texto da Meta</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="textMeta" type="text" placeholder="Sorvete pra ana">
                            <small id="metaHelp2" class="form-text text-muted">Preencher com o texto que vai aparecer na
                                live</small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputMeta" class="col-sm-2 col-form-label">Valor Meta</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="inputMeta" type="number" placeholder="20">
                            <small id="metaHelp" class="form-text text-muted">Colocar sem R$ e inteiro</small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputApurado" class="col-sm-2 col-form-label">Valor Apurado</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="inputApurado" type="number" placeholder="20">
                            <small id="metaHelp3" class="form-text text-muted">Colocar sem R$ e inteiro</small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="button" class="btn btn-primary" id="gravarBtn">Gravar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col-12">
                    Link para o OBS (META): https://gabs.mrguinas.com.br/meta.php
                </div>
                <div class="col-12">
                    Link para o OBS (DOADORES): https://gabs.mrguinas.com.br/usersDonnation.php
                </div>
            </div>
        </div>
</body>

</html>