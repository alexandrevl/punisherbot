$(document).ready(function() {
	setInterval(function() {
		check();
	}, 1000);
});

function check() {
	//console.log("Check...");
	$.ajax({
		type: "POST",
		url: "https://gabs.mrguinas.com.br/getUserDonnation.php",
		contentType: "application/json",
		dataType: "json",
		data: "{ }",
		cache: false,
		statusCode: {
			200: function(response) {
				var rows = "<div class='row'>";
				qntCol = 1;
				//console.log(response);
				response.forEach((user) => {
					if (qntCol > 6) {
						rows += "</div>";
						rows += "<div class='row'>";
						qntCol = 1;
					}
					rows +=
						"<div class='col-2 align-middle text-center' style='color:white; display: inline;'><a href='https://www.youtube.com/channel/" +
						user.channelId +
						"' target='_blank'><img src='" +
						user.avatar +
						"' height=80 class='rounded-circle'></a><br>" +
						user.name +
						"</div>";
					++qntCol;
				});
				rows += "</div>";
				$("#container").html(rows);
			},
			404: function(resultData) {}
		}
	});
}
