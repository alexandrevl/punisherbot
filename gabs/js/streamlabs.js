//Canal GABS
const socketToken =
	"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbiI6IjA4M0FDNDBBNDM5NEFFQjA0M0ZFIiwicmVhZF9vbmx5Ijp0cnVlLCJwcmV2ZW50X21hc3RlciI6dHJ1ZSwieW91dHViZV9pZCI6IlVDQ1lIWkt5TGRVblQ3QnNHZkNRZ1EtUSJ9.Ad1sm-xWo3qFI7Jmm1rk_560FSonvHjx_X1RSk2AE8I"; //Socket token from /socket/token end point

//Connect to socket
const streamlabs = io(`https://sockets.streamlabs.com?token=${socketToken}`);

//Perform Action on event
streamlabs.on("event", (eventData) => {
	console.log(eventData);
	if (eventData.for === "youtube_account" && eventData.type === "superchat") {
		eventData.message.forEach((message) => {
			var donation = message;
			var realAmount = donation.amount / 1000000;
			var user = {
				channelId: donation.channelId,
				channelUrl: donation.channelUrl,
				name: donation.name
			};
			console.log(message);
			var jsonPost = {
				amount: realAmount
			};
			console.log(jsonPost);
			$.ajax({
				type: "GET",
				url: "https://www.googleapis.com/youtube/v3/channels?part=snippet&id=" + donation.channelId + "&key=AIzaSyDMIgGgUIQtpl-MwYEPI0usf4OiK0kQNYU",
				contentType: "application/json",
				dataType: "json",
				cache: false,
				statusCode: {
					200: function(response) {
						user.avatar = response.items[0].snippet.thumbnails.high.url;
						user.mode = "superchat";
						$.ajax({
							type: "POST",
							url: "https://gabs.mrguinas.com.br/addUserDonnation.php",
							contentType: "application/json",
							dataType: "json",
							data: JSON.stringify(user),
							cache: false,
							statusCode: {
								200: function(response) {
									console.log(response.responseText);
								},
								404: function(resultData) {}
							}
						});
					},
					404: function(resultData) {}
				}
			});
			$.ajax({
				type: "POST",
				url: "https://gabs.mrguinas.com.br/addMeta.php",
				contentType: "application/json",
				dataType: "json",
				data: JSON.stringify(jsonPost),
				cache: false,
				statusCode: {
					200: function(response) {
						console.log(response.responseText);
					},
					404: function(resultData) {}
				}
			});
		});
	}
});
