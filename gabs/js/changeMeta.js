$(document).ready(function() {
	check();

	$("#gravarBtn").click(function() {
		gravaMeta();
	});
});
var jsonOld = {};
function check() {
	var jsonPost = {};
	$.ajax({
		type: "POST",
		url: "https://gabs.mrguinas.com.br/getMeta.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				if (!_.isEqual(response, jsonOld)) {
					var percent = Math.round((response.apurado / response.meta) * 100);
					if (percent > 100) {
						percent == 100;
					}
					$("#textMeta").val(response.text);
					$("#inputMeta").val(response.meta);
					$("#inputApurado").val(response.apurado);
					jsonOld = response;
					setTimeout(function() {
						check();
					}, 1000);
				}
			},
			404: function(resultData) {
				setTimeout(function() {
					check();
				}, 1000);
			}
		}
	});
}

function gravaMeta() {
	var textMeta = $("#textMeta").val();
	var inputMeta = $("#inputMeta").val();
	var inputApurado = $("#inputApurado").val();
	$("#alert").attr("class", "alert alert-danger");
	$("#alert").hide();
	if (textMeta.length < 1) {
		$("#alert").text("O texto da meta precisa ser preenchido");
		$("#alert").show();
		return;
	}
	if (inputMeta.length < 1) {
		$("#alert").text("O valor da meta precisa ser preenchido");
		$("#alert").show();
		return;
	}
	if (inputApurado.length < 1) {
		$("#alert").text("O valor apurado precisa ser preenchido");
		$("#alert").show();
		return;
	}
	var jsonPost = {
		text: textMeta,
		meta: inputMeta,
		apurado: inputApurado
	};
	$.ajax({
		type: "POST",
		url: "https://gabs.mrguinas.com.br/setMeta.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				$("#textMeta").val(response.text);
				$("#inputMeta").val(response.meta);
				$("#inputApurado").val(response.apurado);
				$("#alert").text("Meta gravada");
				$("#alert").attr("class", "alert alert-success");
				$("#alert").show();
			},
			404: function(resultData) {}
		}
	});
}
