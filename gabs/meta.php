<html>

<head>
    <title>Alert Box Widget - </title>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="icon" type="image/png" href="../img/Logo.png" />
    <script src="../js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="js/meta.js?version=8.6"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet">
</head>


<body scroll="no" >
    <div align='center' id='default' style='vertical-align: top;margin: 0%; padding: 0%'>
        <!-- <img src="img/default.png" height="45%" width="auto" id='imgTimer' style='vertical-align: top'>-->
    </div>
    <div id="display" style="color:white; display: inline;" align="center"> 
        <div id="textMeta" style="font-size:3.5vw; font-family:'Open Sans'; vertical-align: top; margin: 0%; padding: 0%;-webkit-text-stroke: 2px black;"></div>
        <div class="progress" style="height: 25px;">
            <div id="barP" class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <div id="apuradoText" style="font-size:3vw; font-family:'Open Sans'; vertical-align: top; margin: 0%; padding: 0%;-webkit-text-stroke: 2px black;" class="d-flex justify-content-start"></div>
                </div>
                <div class="col-6">
                    <div id="metaText" style="font-size:3vw; font-family:'Open Sans'; vertical-align: top; margin: 0%; padding: 0%;-webkit-text-stroke: 2px black;" class="d-flex justify-content-end"></div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>