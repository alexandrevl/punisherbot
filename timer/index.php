<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

?>

<html>

<head>
    <title>MrGuinas - Timer - Control</title>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="icon" type="image/png" href="http://mrguinas.com.br/nb/img/Logo.png" />
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js' type='text/javascript'></script>
    <script src="js/streamlabs.js"></script>
    <script src="js/control.js"></script>
    <script src="js/check.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:800" rel="stylesheet">
</head>


<body>
    <br>
    <div class="container">
        <div class="row"> 
            <div class="col-12">
                <div id="display" style="color:black; display: none;" align="center">
                    <div id="timer" style="font-size:7vw; font-family:'Open Sans'; vertical-align: top; margin: 0%; padding: 0%;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <label for="percent">Add Min</label><br>
                <select id="addMin" class="custom-select">
                    <option value="0"></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="30">30</option>
                    <option value="35">35</option>
                    <option value="40">40</option>
                    <option value="45">45</option>
                    <option value="50">50</option>
                    <option value="55">55</option>
                    <option value="60">60</option>
                </select>
            </div>
            <div class="col-2">
                <label for="percent">Sub Min</label><br>
                <select id="subMin" class="custom-select">
                    <option value="0"></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="30">30</option>
                    <option value="35">35</option>
                    <option value="40">40</option>
                    <option value="45">45</option>
                    <option value="50">50</option>
                    <option value="55">55</option>
                    <option value="60">60</option>
                </select>
            </div>
            <div class="col-2">
            </div>
            <div class="col-3">
                <button type="button" class="btn btn-outline-danger" id="resetTimer">Reset Timer</button>
            </div>
            <div class="col-3">
                <button type="button" class="btn btn-outline-danger" id="resetLikes">Reset Likes</button>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col" align="center">
            Link pra colocar no OBS: <a href="http://timer.mrguinas.com.br/popup.html">http://timer.mrguinas.com.br/popup.html</a>
        </div>
    </div>
</body>

</html>