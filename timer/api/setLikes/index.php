<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: text/html; charset=utf-8'); 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

session_start();
$db = "";
$db = new SQLite3('../../timer.db');

$likes = 0;
if(isset($_GET['l'])) {
    $likes = $_GET['l']; 
}

$stmt = $db->prepare("UPDATE timer SET likes = :likes WHERE id=1");
$stmt->bindValue(':likes', $likes, SQLITE3_INTEGER);
$result = $stmt->execute();

echo $likes;

?>