<?php

    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Access-Control-Allow-Origin: *');

    session_start();
    
    $db = new SQLite3('../../timer.db');

    $result = $db->query("select * from timer where id = 1");

    $saida = array();
    while ($row = $result->fetchArray()) {
         $saida['finalTime'] = $row['finalTime'];
    }

    $json = json_encode($saida, JSON_UNESCAPED_UNICODE);
    echo $json;
 
?>