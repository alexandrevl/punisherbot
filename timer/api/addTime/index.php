<?php
header('Content-Type: text/html; charset=utf-8'); 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Access-Control-Allow-Origin: *');

session_start();
$db = ""; 
$db = new SQLite3('../../timer.db');

$min = 0; 
if(isset($_GET['min'])) {
    $min = $_GET['min'];
}

$result = $db->query("select * from timer where id = 1");

$saida = array();
while ($row = $result->fetchArray()) {
        $date = new DateTime();
        $now = $date->getTimestamp()*1000;
        $finalTime = $row['finalTime'];
        if ($now < $finalTime) {
            $minToAdd = $min*60*1000;
            $timer = ($minToAdd + $finalTime);
            $stmt2 = $db->prepare("UPDATE timer SET finalTime = :timer WHERE id=1");
            $stmt2->bindValue(':timer', $timer, SQLITE3_INTEGER);
            $result2 = $stmt2->execute();
            $saida['finalTimer'] = $timer; 
        } else {
            $saida['erro'] = "Time's up"; 
        }
        $json = json_encode($saida, JSON_UNESCAPED_UNICODE);
        echo $json;
}


echo $min;

?>