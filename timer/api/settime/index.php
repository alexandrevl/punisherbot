<?php
header('Content-Type: text/html; charset=utf-8'); 
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Access-Control-Allow-Origin: *');

session_start();
$db = new SQLite3('../../timer.db');

$timer = 0;
if(isset($_GET['t'])) {
    $timer = $_GET['t'];
}

$stmt = $db->prepare("UPDATE timer SET finalTime = :timer WHERE id=1");
$stmt->bindValue(':timer', $timer, SQLITE3_INTEGER);
$result = $stmt->execute();

echo $timer;

?>