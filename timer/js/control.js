$(document).ready(function() {
    $("#resetTimer").click(function() {
        var now = new Date().getTime();
        var target = now + 2 * 60 * 60 * 1000 + 1000;
        setTime(target);
    });
    $("#resetLikes").click(function() {
        resetLikes();
    });
    $("#addMin").change(function() {
        addTime($("#addMin").val());
        $("#addMin").val(0);
    });
    $("#subMin").change(function() {
        addTime($("#subMin").val() * (-1));
        $("#subMin").val(0);
    });
});


function setTime(target) {
    //console.log(target);
    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/timer/api/settime/?t=" + target,
        'uniq_param': (new Date()).getTime(),
        dataType: "text/plain",
    }).fail(function(data) {
        //console.log(data);
    }).done(function(data) {
        console.log("Sucesso");
    });
}

function resetLikes() {
    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/timer/api/setLikes/?l=0",
        'uniq_param': (new Date()).getTime(),
        dataType: "text/plain",
    }).fail(function(data) {
        //console.log(data);
    }).done(function(data) {
        console.log("Sucesso");
    });
    $("#resetLikes").text('Reset Likes (0)');
}


function addTime(min) {
    console.log(min);
    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/timer/api/addTime/?min=" + min,
        'uniq_param': (new Date()).getTime(),
        dataType: "text/plain",
    }).fail(function(data) {
        //console.log(data);
    }).done(function(data) {
        console.log("Sucesso");
    });
}