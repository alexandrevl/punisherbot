//Canal MrGuinas
const socketToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbiI6IkM1NTc4QjRFNTNFNzVCRTRFRkQzIiwicmVhZF9vbmx5Ijp0cnVlLCJwcmV2ZW50X21hc3RlciI6dHJ1ZSwieW91dHViZV9pZCI6IlVDQ0ZjQk9CcjdCM29iNEpqeVRSZFRSUSIsInR3aXRjaF9pZCI6IjE1ODQyNjI2NyJ9.6aaM2a5bHOe6Vf5jupZkrSJrRdQN4SujmtXQxVA2yV8'; //Socket token from /socket/token end point

//Connect to socket
const streamlabs = io(`https://sockets.streamlabs.com?token=${socketToken}`);

//Perform Action on event
streamlabs.on('event', (eventData) => {
    console.log(eventData);
    if (eventData.type === 'alertPlaying' && eventData.message.type === 'superchat') {
        var rawAmount = eventData.message.rawAmount;
        var amount = Math.floor(rawAmount * 2);
        console.log('Superchat Min: ' + amount);
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/timer/api/addTime/?min=" + amount,
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            //console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    }
    if (eventData.type === 'alertPlaying' && eventData.message.type === 'subscription') {
        //var donation = eventData.message[0];
        //console.log(donation.name);
        //console.log(eventData.message[0]);
        var amount = 20;
        console.log('Sponsor Min: ' + amount);
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/timer/api/addTime/?min=" + amount,
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            //console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    }
    if (eventData.type === 'alertPlaying' && eventData.message.type === 'follow') {
        //var donation = eventData.message[0];
        //console.log(donation.name);
        //console.log(eventData.message[0]);
        var amount = 2;
        console.log('Follow Min: ' + amount);
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/timer/api/addTime/?min=" + amount,
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            //console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    }
});