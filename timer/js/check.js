$(document).ready(function() {
    var isSound = true;
    $("#display").show();
    getLikes();

    var distance = 0;
    var distanceOld = 0;

    setInterval(function() {
        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/timer/api/gettime/",
            dataType: "application/json",
            statusCode: {
                200: function(resultData) {
                    var now = new Date().getTime();
                    var timer = JSON.parse(resultData.responseText);

                    distance = timer.finalTime - now;
                    if (distanceOld == 0) {
                        distanceOld = distance;
                    }
                    if (distance > distanceOld) {
                        var diff = distance - distanceOld;
                        var disMin = Math.round((diff % (1000 * 60 * 60)) / (1000 * 60));
                        $('#displayVar').hide().fadeIn(700).delay(1000).fadeOut(700);
                        $('#variation').text('+ ' + disMin + 'min');
                    }
                    distanceOld = distance;
                    if (distance > 0) {
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                        if (seconds < 10) {
                            seconds = "0" + seconds;
                        }
                        if (minutes < 10) {
                            minutes = "0" + minutes;
                        }
                        var textDisplay = hours + ":" + minutes + ":" + seconds;
                        $('#timer').text(textDisplay);
                    } else {
                        $('#timer').text('Time is up');
                    }
                },
                404: function(resultData) {}
            }
        }).fail(function() {
            //console.log("error");
        });
    }, 1000);

    setInterval(function() {
        getLikes();
    }, 60000);
});

function getLikes() {
    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/timer/api/getLikes/",
        dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                var likes = JSON.parse(resultData.responseText);
                var diff = likes.likeCount - likes.likesDB;
                var likeUpdate = Math.floor(diff / 10);
                //console.log(likeUpdate);
                $("#resetLikes").text('Reset Likes (' + likes.likesDB + ')');
                if (likeUpdate > 0) {
                    //setLikes(likeUpdate, likes.likesDB);
                }
                //console.log(likes);
            },
            404: function(resultData) {}
        }
    }).fail(function() {
        //console.log("error");
    });
}

function setLikes(likeUpdate, likesDB) {
    var min = likeUpdate * 4;
    var newLikesDB = likesDB + (likeUpdate * 10);
    console.log('Likes Min: ' + min);
    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/timer/api/setLikes/?l=" + newLikesDB,
        'uniq_param': (new Date()).getTime(),
        dataType: "text/plain",
    }).fail(function(data) {
        //console.log(data);
    }).done(function(data) {
        console.log("Sucesso");
    });

    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/timer/api/addTime/?min=" + min,
        'uniq_param': (new Date()).getTime(),
        dataType: "text/plain",
    }).fail(function(data) {
        //console.log(data);
    }).done(function(data) {
        console.log("Sucesso");
    });
    $("#resetLikes").text('Reset Likes (' + newLikesDB + ')');
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}