<?php 
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    //unlink('mysqlitedb.db');

    include 'Milestone.class.php';
    session_start();
    $db = "";

    if (!isset($_SESSION["db"])) {
        $db = new SQLite3('mysqlitedb.db');
    }
    $index = 0;
    if(isset($_GET['index'])) {
        $index = $_GET['index'];
    }

    $stmt = $db->prepare("UPDATE config SET value = :value WHERE cd_tp=4");
    $stmt->bindValue(':value', $index, SQLITE3_INTEGER);
    $result = $stmt->execute();

    echo $index;

?>