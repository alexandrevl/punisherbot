<?php 
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    include 'access.inc.php'; 
    include 'Stats.class.php';

    session_start();
    $db = "";

    if (!isset($_SESSION["db"])) {
        $db = new SQLite3('mysqlitedb.db');
    }

    $value = 0;
    if(isset($_GET['v'])) {
        $value = $_GET['v'];
    }

    if ($value > 0) { 
        $stmt = $db->prepare("UPDATE config SET value=:value WHERE cd_tp=1");
        $stmt->bindValue(':value', $value, SQLITE3_INTEGER);
        $result = $stmt->execute();
        echo "Chances alteradas para: " . floor((1/$value)*100) . "%";
    } else {
        $limit = 0;
        $result = $db->query("select value from config where cd_tp = 1");
        while ($row = $result->fetchArray()) {
            $limit = $row['value'];
        }
        echo $limit;
    }

?>