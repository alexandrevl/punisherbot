<?php  
include 'access.inc.php'; 
?>
<html>
    <head>
        <title>
            The Punisher - Control Panel
        </title>
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <link rel="icon" type="image/png" href="img/Logo.png" />
        <script src="js/jquery-3.2.1.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/control.js" type="text/javascript"></script>
        <script src="js/stats.js" type="text/javascript"></script>
    </head>
    <body>
        <br>
        <div class="container">
        <div class="card-group">
            <div class="card">
                <div class="card-block">
                <h5 class="card-title"><a href="#" id="tentativas" data-toggle="modal" data-target="#listModal"><i class="fa fa-list fa-fw" aria-hidden="true"></i></a>&nbsp;Tentativas</h5>
                <h3 class="card-text" id="qntCooldown">0</h3>
                </div>
            </div>
            <div class="card">
                <div class="card-block">
                <h5 class="card-title"><a href="#" id="punicoes" data-toggle="modal" data-target="#listModal"><i class="fa fa-list fa-fw" aria-hidden="true"></i></a>&nbsp;Punições</h5>
                <h3 class="card-text" id="qntPunishments">0</h3>
                </div>
            </div>
            <div class="card">
                <div class="card-block">
                <h5 class="card-title"><a href="#" id="queue" data-toggle="modal" data-target="#listModal"><i class="fa fa-list fa-fw" aria-hidden="true"></i></a>&nbsp;Na Fila</h5>
                <h3 class="card-text" id="qntQueue">0</h3>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-3">
                    <label for="user">Nome do usuário</label> <button type="button" class="btn btn-link" id="randomName">Random</button>
                    <input type="text" class="form-control" id="user" aria-describedby="user" value="UsuarioTeste">
                </div>
                <div class="col-2">
                    <label for="percent">Chances de acerto</label><br>
                    <select id="limit" class="custom-select">
                        <option value="1">100%</option>
                        <option value="2">50%</option>
                        <option value="3">33%</option>
                        <option value="4">25%</option>
                        <option value="5">20%</option>
                        <option value="6">16%</option>
                        <option value="7">14%</option>
                        <option value="8">12%</option>
                        <option value="9">11%</option>
                        <option value="10" selected>10%</option>
                        <option value="12">8%</option>
                        <option value="20">5%</option>
                    </select>
                </div>
                <div class="col-3">
                <label for="punishemnts"><a href="#" id="statPun" data-toggle="modal" data-target="#listModal"><i class="fa fa-list fa-fw" aria-hidden="true"></i></a>&nbsp;Punições</label><br>
                    <button type="button" class="btn btn-outline-info" id="configPunishments">Configurar Punições</button>
                </div>
                <div class="col-3">
                <label for="milestones">Milestones</label><br>
                    <button type="button" class="btn btn-outline-info" id="configMilestones">Controle Milestones</button>
                </div>
            </div>
            <div class="row">
            <br>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-2">
                    <button type="button" class="btn btn-primary" id="normal">Normal (!punir)</button>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-success" id="forcar">Forçar (!punir)</button>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-success" id="doacao">Forçar Doação</button>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-warning" id="matar">Matar Punição</button>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-danger" id="dialogResetar" data-toggle="modal" data-target="#resetar">Resetar base</button>
                </div>
            </div>
            <div class="row">
                <br>
            </div>  
            <div class="row">
                <div class="col">
                    <div class="alert alert-info" role="alert" id="alert">
                        Ready... Os comandos vão aparecer aqui ;)
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <iframe src="http://mrguinas.com.br/nb/popup.html?sound=1" width="100%" height="400" frameBorder="0" scrolling="no">Browser not compatible. </iframe >
                        <!--
                        <iframe src="http://localhost:81/mrguinas/popup.html?sound=1" width="100%" height="400" frameBorder="0" scrolling="no">Browser not compatible. </iframe >
                    -->
                </div>
            </div>
            <div class="row">
                <div class="col">
                    Link pra colocar no OBS: <a href="http://mrguinas.com.br/nb/popup.html">http://mrguinas.com.br/nb/popup.html</a>
                </div>
            </div>
        </div>
        <div class="modal fade" id="resetar" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Resetar base de dados</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tem certeza que deseja resetar TODOS os dados?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="OkResetar" data-dismiss="modal">Sim, exclui tudo</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não, fecha isso</button>
                </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="listModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titleModal">Lista</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped" id="listTable">
                        <thead id="thead">
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
                </div>
            </div>
        </div>
    </body>
</html>