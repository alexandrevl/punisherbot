<?php 
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    //unlink('mysqlitedb.db');

    include 'Punishment.class.php';

    $isPunished = false;
    $isCooldown = false;    
    $punisher = "";
    $limit = 8;  
    $target = 1;

    $choiceTimeMin = 1;
    $cooldownTimeSec = 20;
    $user = "default";
    $channelId = "";
    
    $super = false;
    $source = 0;

    if(isset($_GET['n'])) {
        $target = $_GET['n'];
    }
    if(isset($_GET['u'])) {
        $user = $_GET['u'];
    }
    if(isset($_GET['s'])) {
        $super = true;
        $source = $_GET['s'];
    }
    if(isset($_GET['c'])) {
        $channelId = $_GET['c'];
    }

    session_start();
    $db = "";

    if (!isset($_SESSION["db"])) {
        $db = new SQLite3('mysqlitedb.db');
    }

    $result = $db->query("select value from config where cd_tp = 1");
    while ($row = $result->fetchArray()) {
        $limit = intval($row['value']);
    }
    $num = rand(1,$limit);

    $stmt = $db->prepare("select * from cooldown where user = :user order by timestamp desc LIMIT 1");
    //$stmt->bindValue(':today',time(),SQLITE3_INTEGER);
    $stmt->bindValue(':user',$user,SQLITE3_TEXT);
    $result =  $stmt->execute();

    while ($row = $result->fetchArray()) {
        $timestamp = $row['timestamp'];
        $now = time();
        if (($now-$timestamp) <= $cooldownTimeSec) {
            $isCooldown = true;
        }
    }
    if ($super) {
        $isCooldown = false;
        $target = $num;
    } else {
        $stmt = $db->prepare("select * from cooldown where user = :user and is_punished = 1 order by timestamp desc LIMIT 1");
        //$stmt->bindValue(':today',time(),SQLITE3_INTEGER);
        $stmt->bindValue(':user',$user,SQLITE3_TEXT);
        $result =  $stmt->execute();
        while ($row = $result->fetchArray()) {
            $timestamp = $row['timestamp'];
            $now = time();
            if (($now-$timestamp) >= 600) {
                $limit = floor($limit/3);
            } else if (($now-$timestamp) >= 300) {
                $limit = floor($limit/2);
            }
        }
    }
    if (!$isCooldown) {
        $ts_fim = 0;
        $result = $db->query("select * from punishment where ts_fim > " . time(). " and ts_ini < " . time());
        while ($row = $result->fetchArray()) {
            $punisher = $row['user'];
            $ts_fim = $row['ts_fim'];
            $isPunished = true;
        }
        $phrasesOK = array(
            "Ah seu verme!! Tá na mão!",
            "Muahahaha!",
            "Olha ele, olha ele!",
            "Boom!! Headshot!!!"
            
        );
        $phrasesNOK = array(
            "Errrrrroouuu!",
            "Ih... não rolou!",
            "Não não não não... errou",
            "Errou feio, errou rude...",
            "Hoje sim, hoje sim, só que não...",
            "Não desista, foi por pouco!",
            "Putz, quase...",
            "Que bom que vc ta tentando... mas não deu.",
            "Não deu! Não desista dos seus sonhos..",
            "Um dia vc acerta, mas agora não.",
            "Sorte no amor, azar no !punir... errou!",
            "Vc errou, te falta determinação!",
            "A lua de júpiter não está te ajudando, vc errou de novo!",
            "Sinônimo de fracasso: suas tentativas no !punir.",
            "Eu não queria fazer bullying, mas vc errou de novo!",
            "Na traaaaaaaaaaave"
        );

        $okPunishment = 0;
        if (!$isPunished) {
            if ($num == $target) {
                $punishment = new Punishment();
                $punishment->punisher = $user;
                $punishment->ts_ini = time();
                $punishment->cod_pun = 1;
                $punishment->source = $source;
                $punishment->channel_id = $channelId;

                $txtPun = addPunishment($db, $punishment);
                $stmt = $db->prepare("select count(id) as total from punishment where user = :user");
                $stmt->bindValue(':user',$user,SQLITE3_TEXT);
                $result =  $stmt->execute();
                $total = 0;
                while ($row = $result->fetchArray()) {
                    $total = intval($row['total']);
                }
                echo "" . $phrasesOK[rand(0,(sizeof($phrasesOK)-1))] . " " . $punishment->punisher . " (". $total ."a punição sua hoje). Punição para MrGuinas: " . $txtPun;
                $okPunishment = 1;
            } else {
                echo "" .$phrasesNOK[rand(0,(sizeof($phrasesNOK)-1))] . " " . $user;
            }
            //echo " " . $target . "/" . $num . " ";
        } else {
            if ($super) {
                $isUserPunishingActive = false;
                $stmt = $db->prepare("select * from punishment where user = :user and ts_fim > " . time());
                $stmt->bindValue(':user',$user,SQLITE3_TEXT);
                $result =  $stmt->execute();
                $txt = "";
                while ($row = $result->fetchArray()) {
                    $txt = $row['tx_pun'];
                    $isUserPunishingActive = true;
                }

                if (!$isUserPunishingActive) {
                    $result = $db->query("select * from punishment order by ts_fim desc limit 1");
                    while ($row = $result->fetchArray()) {
                        $ts_fim = $row['ts_fim'];
                    }
                    $punishment = new Punishment();
                    $punishment->punisher = $user;
                    $punishment->ts_ini = $ts_fim+1;
                    $punishment->cod_pun = 1;
                    $punishment->source = $source;
                    $punishment->channel_id = $channelId;

                    $txt = addPunishment($db, $punishment);

                    echo "Punição na fila de ". $user. ": " . $txt;
                } else {
                    echo "Já existe uma punição na fila para ". $user. ": " . $txt;
                }
            } else {
                if ($user == $punisher) {
                    echo "". $user . ", ele já está sendo punido por você... espere um pouco ;)";
                } else {
                    echo "". $user . ", ele já está sendo punido por " . $punisher . "... espere um pouco ;)";
                }
            }
        }  
        $stmt = $db->prepare("INSERT INTO cooldown (user, timestamp, is_punished, channel_id) VALUES (:user, :ts, :is_punished, :channel_id)");
        $stmt->bindValue(':user', $user, SQLITE3_TEXT);
        $stmt->bindValue(':ts', time(), SQLITE3_INTEGER);
        $stmt->bindValue(':is_punished', $okPunishment, SQLITE3_INTEGER);
        $stmt->bindValue(':channel_id', $channelId, SQLITE3_TEXT);
        $result = $stmt->execute(); 
    } else {
        echo "Calma ai " . $user . ". Daqui a pouco vc pode tentar de novo. (" . $cooldownTimeSec . "s)";
    }

    function addPunishment($db, $punData) {
        $punishmentsSettings = array(
            //Max 16 char
            //array("dropar um item",30),
            // array("5 min s/ save",5*60),
            // array("tirar o chão",60),
            // array("sem correr",60),
            // array("emulação 240%",60),
            // array("emulação 240%",60),
            // array("emulação 240%",60),
            // array("mate 1 macaco",20),
            // array("volte 1 fase",20),
            // array("volte 2 fases",20),
            // array("morrer",20),
            // array("speedrun",60),
            // array("inverter comandos",60),
            // array("inverter mãos",60), 
            // array("Donkey Kong",20),
            // array("Donkey Kong 2",20),
            // array("Donkey Kong 3",20), 
            // array("bônus ou 3 vidas",60),
            // array("reset",20),
            // array("pacifista",60),
            // array("sem personagens",60),
            // array("sem personagens",60),
            // array("sem personagens",60),
            // array("volte 1 save",20)

            array("Fácil",array(200,5)),
            array("teste mais dificil",array(200,1)),
        );
        $punishmentsSettings = array();
        $result = $db->query("select * from config where cd_tp = 2");
        while ($row = $result->fetchArray()) {
            $value = json_decode($row['value'],true);
            array_push($punishmentsSettings,array($value['tx_pun'],array($value['seconds'],$value['iForce'])));
        }

        $punishments = array();
        foreach ($punishmentsSettings as $pun) {
            $punSettings = $pun[1];
            $iPun = 1;
            while ($iPun <= $punSettings[1]) {
                array_push($punishments, array($pun[0],$punSettings[0]));
                //echo $pun[0] . " - " . $punSettings[0];
                ++$iPun;
            }
        }
        //echo implode(",", $punishments);

        $index_pun = rand(0,(sizeof($punishments)-1));
        $tx_pun = $punishments[$index_pun][0];
        $tsPunishment = $punData->ts_ini + ($punishments[$index_pun][1]+2);
        $stmt = $db->prepare("INSERT INTO punishment (user, ts_ini, cod_pun, tx_pun, source, ts_fim, channel_id) VALUES (:user, :ts_ini, :cod_pun, :tx_pun, :source, :ts_fim, :channel_id)");
        $stmt->bindValue(':user', $punData->punisher, SQLITE3_TEXT);
        $stmt->bindValue(':ts_ini', $punData->ts_ini, SQLITE3_INTEGER);
        $stmt->bindValue(':cod_pun', $punData->cod_pun, SQLITE3_INTEGER);
        $stmt->bindValue(':tx_pun', $tx_pun, SQLITE3_TEXT);
        $stmt->bindValue(':source', $punData->source, SQLITE3_TEXT);
        $stmt->bindValue(':ts_fim', $tsPunishment, SQLITE3_INTEGER);
        $stmt->bindValue(':channel_id', $punData->channel_id, SQLITE3_TEXT);
        $result = $stmt->execute();

        return $tx_pun;
    }
    //echo " +=+ " . $limit . "- " . $num;
?> 