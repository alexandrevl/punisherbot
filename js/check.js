$(document).ready(function() {
    var isSound = true;

    var url = new URL(location.href);
    var sound = url.searchParams.get("sound");
    //var sound = 0;
    if (sound == 1) {
        isSound = false;
    }
    var isPrimeira = false;
    setInterval(function() {
        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/s.php",
            //url: "http://localhost:81/mrguinas/s.php",
            //data: jsonPost2,
            dataType: "application/json",
            // error: function(resultData) {
            //     console.log(resultData);
            // },
            statusCode: {
                200: function(resultData) {
                    //console.log('Foi ');
                    //console.log(resultData.responseText);
                    var punishment = JSON.parse(resultData.responseText);
                    //console.log(punishment);
                    var now = new Date().getTime();
                    distance = Number(punishment.ts_fim * 1000) - now;

                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = hours * 60 + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    if (seconds < 10) {
                        seconds = "0" + seconds;
                    }
                    if (punishment.source > 0) {
                        $("#display").css('color', 'yellow');
                    }
                    $("#default").hide();
                    if (punishment.channel_id != "") {
                        $('#imgPunisher').height("30%");
                        $("#imgPunisher").css('border-radius', '50%');
                        $("#imgPunisher").attr("src", punishment.thumb_url);
                    } else {
                        $('#imgPunisher').height("20%");
                        $("#imgPunisher").css('border-radius', '0px');
                        $("#imgPunisher").attr("src", "img/punisherlogo.png");
                    }
                    $("#display").show();
                    $("#text").text(capitalizeFirstLetter(punishment.tx_pun));
                    var showPunisher = punishment.punisher;
                    if (showPunisher.length > 18) {
                        showPunisher = showPunisher.substring(0, 16) + "...";
                    }
                    $("#punisher").text(showPunisher);
                    if (distance > 100) {
                        $("#countdown").text("(" + minutes + ":" + seconds + ")");
                    }
                    if (!isPrimeira) {
                        isPrimeira = true;
                        if (isSound) {
                            var audio = new Audio('sound/Big_Explosion_Cut_Off.mp3');
                            audio.volume = 0.2;
                            audio.play();
                        }

                    }
                },
                404: function(resultData) {
                    if (isPrimeira) {
                        if (isSound) {
                            var audio = new Audio('sound/Beep_Short.mp3');
                            audio.volume = 0.2;
                            audio.play();
                        }
                    }
                    $("#display").css('color', 'white');
                    isPrimeira = false;
                    $("#display").hide();
                    $("#default").show();
                    //console.log("Nao foi");
                }
            }
        }).fail(function() {
            //console.log("error");
        });
    }, 1000);
});

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}