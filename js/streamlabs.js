//Canal MrGuinas
const socketToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbiI6IkM1NTc4QjRFNTNFNzVCRTRFRkQzIiwicmVhZF9vbmx5Ijp0cnVlLCJwcmV2ZW50X21hc3RlciI6dHJ1ZSwieW91dHViZV9pZCI6IlVDQ0ZjQk9CcjdCM29iNEpqeVRSZFRSUSIsInR3aXRjaF9pZCI6IjE1ODQyNjI2NyJ9.6aaM2a5bHOe6Vf5jupZkrSJrRdQN4SujmtXQxVA2yV8'; //Socket token from /socket/token end point

//Connect to socket
const streamlabs = io(`https://sockets.streamlabs.com?token=${socketToken}`);

//Perform Action on event
streamlabs.on('event', (eventData) => {
    if (!eventData.for || eventData.for === 'streamlabs' && eventData.type === 'donation') {
        //code to handle donation events
        var donation = eventData.message[0];
        //console.log(donation.name);
        console.log(eventData.message[0]);
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/t.php?u=" + donation.name + "&s=1",
            //url: "http://localhost:81/mrguinas/t.php?u=" + donation.name + "&s=1",
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain",
        }).fail(function(data) {
            console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    }
    if (!eventData.for || eventData.for === 'youtube_account' && eventData.type === 'superchat') {
        var donation = eventData.message[0];
        //console.log(donation.name);
        console.log(eventData.message[0]);
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/t.php?u=" + donation.name + "&s=2",
            //url: "http://localhost:81/mrguinas/t.php?u=" + donation.name + "&s=2",
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    }
});