$(document).ready(function() {
    var md5Old = "";
    setInterval(function() {

        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/stats.php",
            //url: "http://localhost:81/mrguinas/stats.php",
            //data: jsonPost2,
            dataType: "application/json",
            // error: function(resultData) {
            //     console.log(resultData);
            // },
            statusCode: {
                200: function(resultData) {
                    var stats = JSON.parse(resultData.responseText);
                    //console.log(stats);
                    var md5New = stats.md5;
                    if (md5New != md5Old) {
                        $("#qntCooldown").text(stats.qntCooldown);
                        $("#qntPunishments").text(stats.qntPunishments);
                        $("#qntQueue").text(stats.qntQueue);
                        $("#configPunishments").text("Configurar Punições (" + stats.qntListPunishments + ")");
                        $('#limit').val(stats.limit);
                        md5Old = md5New;
                    }
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    }, 1000);

});