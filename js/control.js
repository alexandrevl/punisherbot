$(document).ready(function() {

    //var url = "http://localhost:81/mrguinas/";
    var url = "http://mrguinas.com.br/nb/";

    $("#configPunishments").click(function() {
        console.log("Alow");
        var url2 = url + "config.php";
        var win = window.open(url2, 'punir');
        win.focus();
    });

    $("#configMilestones").click(function() {
        var url2 = url + "milestones.php";
        var win = window.open(url2, 'milestones');
        win.focus();
    });

    $("#limit").change(function() {
        $("#alert").text("");
        $.ajax({
            type: 'GET',
            url: url + "update.php?v=" + $("#limit").val(),
            dataType: "application/json",
            statusCode: {
                200: function(resultData) {
                    $("#alert").text("[% Acertos] " + resultData.responseText);
                    $("#alert").attr('class', 'alert alert-success');
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });

    $("#normal").click(function() {
        $("#alert").text("");
        $.ajax({
            type: 'GET',
            url: url + "t.php?u=" + $("#user").val(),
            dataType: "application/json",
            statusCode: {
                200: function(resultData) {
                    $("#alert").text("[Normal - !punir] " + resultData.responseText);
                    $("#alert").attr('class', 'alert alert-success');
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });

    $("#forcar").click(function() {
        $("#alert").text("");
        $.ajax({
            type: 'GET',
            url: url + "t.php?u=" + $("#user").val() + "&s=0",
            dataType: "application/json",
            statusCode: {
                200: function(resultData) {
                    $("#alert").text("[Forcado - !punir] " + resultData.responseText);
                    $("#alert").attr('class', 'alert alert-success');
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });

    $("#doacao").click(function() {
        $("#alert").text("");
        $.ajax({
            type: 'GET',
            url: url + "t.php?u=" + $("#user").val() + "&s=1",
            dataType: "application/json",
            statusCode: {
                200: function(resultData) {
                    $("#alert").text("[Doacao] " + resultData.responseText);
                    $("#alert").attr('class', 'alert alert-success');
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });

    $("#matar").click(function() {
        $("#alert").text("");
        $.ajax({
            type: 'GET',
            url: url + "r.php?o=1",
            dataType: "application/json",
            statusCode: {
                200: function(resultData) {
                    $("#alert").text("[Matar Punicao] " + resultData.responseText);
                    $("#alert").attr('class', 'alert alert-warning');
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });

    $("#OkResetar").click(function() {
        $("#alert").text("");
        $.ajax({
            type: 'GET',
            url: url + "r.php?o=0",
            dataType: "application/json",
            statusCode: {
                200: function(resultData) {
                    $("#alert").text("[Reset geral] " + resultData.responseText);
                    $("#alert").attr('class', 'alert alert-warning');
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });
    $('#dialogResetar').on('shown.bs.modal', function() {
        $('#myInput').focus();
    });
    $('#tentativas').click(function() {
        $("#tbody").empty();
        $('#listTable > thead').replaceWith(
            '<tr>' +
            '<th>#</th>' +
            '<th>User</th>' +
            '<th>Quantidade</th>' +
            '</tr>');

        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/lists.php",
            //url: "http://localhost:81/mrguinas/listPunishment.php",
            //data: jsonPost2,
            dataType: "application/json",
            // error: function(resultData) {
            //     console.log(resultData);
            // },
            statusCode: {
                200: function(resultData) {
                    var json = JSON.parse(resultData.responseText);
                    var coolDown = json.usersCooldown;
                    $('#titleModal').text("Ranking tentativas (" + json.qntCooldown + ")");
                    var i = 1;
                    coolDown.forEach(function(users) {
                        $('#listTable > tbody:last-child').append(
                            '<tr>' +
                            '<th scope="row">' + i++ + '</th>' +
                            '<td>' + users.user + '</td>' +
                            '<td>' + users.qnt + '</td>' +
                            '</tr>');
                    }, this);
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });
    $('#punicoes').click(function() {
        $("#tbody").empty();
        $('#listTable > thead').replaceWith(
            '<tr>' +
            '<th>#</th>' +
            '<th>User</th>' +
            '<th>Quantidade</th>' +
            '</tr>');

        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/lists.php",
            //url: "http://localhost:81/mrguinas/listPunishment.php",
            //data: jsonPost2,
            dataType: "application/json",
            // error: function(resultData) {
            //     console.log(resultData);
            // },
            statusCode: {
                200: function(resultData) {
                    var json = JSON.parse(resultData.responseText);
                    var punishments = json.usersPunishments;
                    $('#titleModal').text("Ranking Punições por user  (" + json.qntPunishments + ")");
                    var i = 1;
                    punishments.forEach(function(users) {

                        $('#listTable > tbody:last-child').append(
                            '<tr>' +
                            '<th scope="row">' + i++ + '</th>' +
                            '<td>' + users.user + '</td>' +
                            '<td>' + users.qnt + '</td>' +
                            '</tr>');
                    }, this);
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });

    $('#statPun').click(function() {
        $("#tbody").empty();

        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/lists.php",
            //url: "http://localhost:81/mrguinas/listPunishment.php",
            //data: jsonPost2,
            dataType: "application/json",
            // error: function(resultData) {
            //     console.log(resultData);
            // },
            statusCode: {
                200: function(resultData) {
                    var json = JSON.parse(resultData.responseText);
                    var punishments = json.statPun;
                    var qntPunishments = json.qntPunishments;
                    $('#titleModal').text("Ranking Punições  (" + json.qntPunishments + ")");
                    $('#listTable > thead').replaceWith(
                        '<tr>' +
                        '<th>#</th>' +
                        '<th>Punição</th>' +
                        '<th>Quantidade</th>' +
                        '</tr>');
                    var i = 1;
                    punishments.forEach(function(statPun) {
                        $('#listTable > tbody:last-child').append(
                            '<tr>' +
                            '<th scope="row">' + i++ + '</th>' +
                            '<td>' + statPun.tx_pun + '</td>' +
                            '<td>' + statPun.qnt + ' (' + Math.round((statPun.qnt / qntPunishments) * 100) + '%)</td>' +
                            '</tr>');
                    }, this);
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });

    $('#queue').click(function() {
        $("#tbody").empty();
        $.ajax({
            type: 'GET',
            url: "http://mrguinas.com.br/nb/lists.php",
            //url: "http://localhost:81/mrguinas/listPunishment.php",
            //data: jsonPost2,
            dataType: "application/json",
            // error: function(resultData) {
            //     console.log(resultData);
            // },
            statusCode: {
                200: function(resultData) {
                    $('#listTable > thead').replaceWith(
                        '<tr>' +
                        '<th>#</th>' +
                        '<th>User</th>' +
                        '<th>Texto</th>' +
                        '</tr>');
                    var json = JSON.parse(resultData.responseText);
                    var punishments = json.queue;
                    $('#titleModal').text("Punições na fila (" + json.qntQueue + ")");
                    var i = 1;
                    punishments.forEach(function(pun) {
                        $('#listTable > tbody:last-child').append(
                            '<tr>' +
                            '<th scope="row">' + i++ + '</th>' +
                            '<td>' + pun.user + '</td>' +
                            '<td>' + pun.tx_pun + '</td>' +
                            '</tr>');
                    }, this);
                },
                404: function(resultData) {

                }
            }
        }).fail(function() {
            //console.log("error");
        });
    });

    $("#randomName").click(function() {

        var names = ["Saab", "Volvo", "BMW", "Alexandre Lima", "Ana Danielle", "Robson", "Diogo", "Chapeco", "Samuel", "Vitor Lim", "Hell", "Alcapone", "Skyrasgo", "Lovis", "Punisher", "AzulMano", "NaoLigo", "NossaMeu"];

        var indexName = Math.floor(Math.random() * names.length);

        $("#user").val(names[indexName]);
    });

});