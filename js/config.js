var md5Old = "";
$(document).ready(function() {
    setInterval(function() {
        updateTable();
    }, 1000);
    $('#myModal').on('shown.bs.modal', function() {
        $('#tx_pun').focus();
    });
    $("#savePunishment").click(function() {
        //console.log("Ok");
        var punishmentList = {};
        punishmentList.tx_pun = $("#tx_pun").val();
        punishmentList.seconds = $("#tempo").val();
        punishmentList.iForce = $("#forca").val();

        console.log(punishmentList);
        if (punishmentList.tx_pun != "") {
            $.ajax({
                type: 'POST',
                url: "http://mrguinas.com.br/nb/updatePunishment.php",
                //url: "http://localhost:81/mrguinas/updatePunishment.php",
                data: JSON.stringify(punishmentList),
                //dataType: "json",
                // error: function(resultData) {
                //     console.log(resultData);
                // },
                statusCode: {
                    200: function(resultData) {
                        $("#tx_pun").val("");
                        $("#tempo").val(10);
                        $("#forca").val(1);
                        console.log("Ok");
                        console.log(resultData.responseText);
                        updateTable();
                    },
                    404: function(resultData) {
                        console.log("erro");
                    }
                }
            }).fail(function() {
                // console.log("error");
            });
        }
        $('#myModal').modal('hide');
    });
});

function deletePunishment(id) {
    //console.log("Aloe " + id);
    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/deletePunishment.php?id=" + id,
        //url: "http://localhost:81/mrguinas/deletePunishment.php?id=" + id,
        //data: jsonPost2,
        dataType: "application/json",
        // error: function(resultData) {
        //     console.log(resultData);
        // },
        statusCode: {
            200: function(resultData) {
                updateTable();
            },
            404: function(resultData) {

            }
        }
    }).fail(function() {
        //console.log("error");
    });
}

function updateTable() {
    var md5New = md5Old;

    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/listPunishment.php",
        dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                var json = JSON.parse(resultData.responseText);
                md5New = json.md5;
                //console.log(md5Old);
                //console.log(md5New);
                if (md5Old != md5New) {
                    $("#tbody").empty();
                    md5Old = md5New;
                    var listPunishment = json.punishmentList;
                    var totForce = 0;
                    listPunishment.forEach(function(punishment) {
                        totForce += parseInt(punishment.iForce);
                    }, this);
                    var i = 1;
                    listPunishment.forEach(function(punishment) {
                        $('#listPunishments > tbody:last-child').append(
                            '<tr>' +
                            '<th scope="row">' + i++ + '</th>' +
                            '<td>' + punishment.tx_pun + '</td>' +
                            '<td>' + punishment.seconds + '</td>' +
                            '<td>' + punishment.iForce + ' (' + Math.round((punishment.iForce / totForce) * 100) + '%)</td>' +
                            '<td class="align: center"><button type="button" class="btn btn-outline-danger btn-sm" id="matar" value="' + punishment.id + '" onclick="deletePunishment(' + punishment.id + ')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>' +
                            '</tr>');

                    }, this);
                }
            },
            404: function(resultData) {}
        }
    }).fail(function() {
        //console.log("error");
    });
}