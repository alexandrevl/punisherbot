var md5Old = "";
var index = 0;
$(document).ready(function() {
    setInterval(function() {
        var t = updateTable();
    }, 1000);
});

document.onkeyup = function(e) {
    var e = e || window.event; // for IE to cover IEs window event-object
    if (e.which == 34) {
        alert('Pra baixo');
        return false;
    }
    if (e.which == 33) {
        alert('Pra cima');
        return false;
    }
    if (e.which == 40) {
        nextMilestone();
        return false;
    }
    if (e.which == 38) {
        prevMilestone();
        return false;
    }
    if (e.which == 82) {
        resetMilestone();
        return false;
    }
}

var listMilestones = {};

function resetMilestone() {
    updateEntry(0);
}

function nextMilestone() {
    updateEntry(++index);
}

function prevMilestone() {
    updateEntry(--index);
}

function updateEntry(i) {
    var url = "http://mrguinas.com.br/nb/updateMilestoneStatus.php?index=" + i;
    //console.log(url);
    $.ajax({
        type: 'GET',
        url: url,
        //ataType: "application/json",
        statusCode: {
            200: function(resultData) {
                //console.log(resultData);
                index = resultData;
            },
            404: function(resultData) {
                console.log("erro");
            }
        }
    }).fail(function() {
        // console.log("error");
    });
}

function updateTable() {
    var md5New = md5Old;
    var listMilestones = {};

    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/listMilestones.php",
        dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                var json = JSON.parse(resultData.responseText);
                md5New = json.md5;
                //console.log(md5Old);
                //console.log(md5New);
                //console.log(json);
                index = json.index;
                listMilestones = json.milestonesList;
                if (md5Old != md5New) {
                    $("#tbody").empty();
                    md5Old = md5New;
                    var totForce = 0;
                    var i = 1;
                    listMilestones.forEach(function(milestone) {
                        $('#listPunishments > tbody:last-child').append(
                            '<tr>' +
                            '<th scope="row">' + i++ + '</th>' +
                            '<td>' + milestone.tx_milestone + '</td>' +
                            '<td class="align: center">' + milestone.status + '</td>' +
                            '</tr>');

                    }, this);
                }
            },
            404: function(resultData) {}
        }
    }).fail(function() {
        //console.log("error");
    });
}