var md5Old = "";
var qtMilestones = 0;
var index = 0;
var maxIndex = 0;
$(document).ready(function() {
    updateTable();
    setInterval(function() {
        updateTable();
    }, 1000);
    $('#myModal').on('shown.bs.modal', function() {
        $('#tx_pun').focus();
    });
    $("#prev").click(function() {
        prevMilestone();
    });
    $("#next").click(function() {
        nextMilestone();
    });
    $("#reset").click(function() {
        resetMilestone();
    });
    $("#saveMilestone").click(function() {
        //console.log("Ok");
        var milestoneList = {};
        milestoneList.tx_milestone = $("#tx_milestone").val();
        milestoneList.status = 0;
        milestoneList.index = qtMilestones + 1;

        console.log(milestoneList);
        if (milestoneList.tx_milestone != "") {
            $.ajax({
                type: 'POST',
                url: "http://mrguinas.com.br/nb/updateMilestone.php",
                //url: "http://localhost:81/mrguinas/updatePunishment.php",
                data: JSON.stringify(milestoneList),
                //dataType: "json",
                // error: function(resultData) {
                //     console.log(resultData);
                // },
                statusCode: {
                    200: function(resultData) {
                        $("#tx_milestone").val("");
                        console.log("Ok");
                        console.log(resultData.responseText);
                        updateTable();
                    },
                    404: function(resultData) {
                        console.log("erro");
                    }
                }
            }).fail(function() {
                // console.log("error");
            });
        }
        $('#myModal').modal('hide');
    });
});

function deleteMilestone(id) {
    //console.log("Aloe " + id);
    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/deleteMilestone.php?id=" + id,
        //url: "http://localhost:81/mrguinas/deletePunishment.php?id=" + id,
        //data: jsonPost2,
        //dataType: "application/json",
        // error: function(resultData) {
        //     console.log(resultData);
        // },
        statusCode: {
            200: function(resultData) {
                //console.log(resultData);
                updateTable();
            },
            404: function(resultData) {

            }
        }
    }).fail(function() {
        //console.log("error");
    });
}

function updateTable() {
    var md5New = md5Old;

    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/listMilestones.php",
        dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                var json = JSON.parse(resultData.responseText);
                md5New = json.md5;
                if (md5Old != md5New) {
                    $("#tbody").empty();
                    md5Old = md5New;
                    var listMilestones = json.milestonesList;
                    index = json.index;
                    var i = 0;
                    listMilestones.forEach(function(milestone) {
                        classActive = "";
                        if (i == index) {
                            classActive = ' class="table-success"';
                        }
                        $('#listPunishments > tbody:last-child').append(
                            '<tr' + classActive + '>' +
                            '<th scope="row"><a href="#" onclick="updateEntry(' + i + ')"> ' + i++ + '</a></th>' +
                            '<td>' + milestone.tx_milestone + '</td>' +
                            // '<td>' + punishment.seconds + '</td>' +
                            // '<td>' + punishment.iForce + ' (' + Math.round((punishment.iForce / totForce) * 100) + '%)</td>' +
                            '<td class="align: center"><button type="button" class="btn btn-outline-danger btn-sm" id="matar" value="' + milestone.id + '" onclick="deleteMilestone(' + milestone.id + ')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>' +
                            '</tr>');

                    }, this);
                    maxIndex = (i - 1);
                }
            },
            404: function(resultData) {}
        }
    }).fail(function() {
        //console.log("error");
    });
}

document.onkeyup = function(e) {
    var e = e || window.event; // for IE to cover IEs window event-object
    if (e.which == 39 || e.which == 40 || e.which == 34) {
        nextMilestone();
        return false;
    }
    if (e.which == 37 || e.which == 48 || e.which == 33) {
        prevMilestone();
        return false;
    }
    if (e.which == 40) {
        nextMilestone();
        return false;
    }
    if (e.which == 38) {
        prevMilestone();
        return false;
    }
    if (e.which == 82) {
        resetMilestone();
        return false;
    }
}

var listMilestones = {};

function resetMilestone() {
    updateEntry(0);
}

function nextMilestone() {
    if (index < maxIndex) {
        updateEntry(++index);
    }
}

function prevMilestone() {
    if (index > 0) {
        updateEntry(--index);
    }
}

function updateEntry(i) {
    console.log(i);
    var url = "http://mrguinas.com.br/nb/updateMilestoneStatus.php?index=" + i;
    //console.log(url);
    $.ajax({
        type: 'GET',
        url: url,
        //ataType: "application/json",
        statusCode: {
            200: function(resultData) {
                //console.log(resultData);
                index = resultData;
                updateTable();
            },
            404: function(resultData) {
                console.log("erro");
            }
        }
    }).fail(function() {
        // console.log("error");
    });
}