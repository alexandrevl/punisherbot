$(document).ready(function() {
    $("#signIn").click(function() {
        login();
    });
    $("#inputPassword").keypress(function(e) {
        var key = e.which;
        if (key == 13) {
            login();
        }
    });
});

function login() {
    $("#alert").hide();
    var passhash = md5($("#inputPassword").val());

    $.ajax({
        type: 'POST',
        url: "http://mrguinas.com.br/nb/checkLogin.php?p=" + passhash,
        //url: "http://localhost:81/mrguinas/updatePunishment.php",
        //data: JSON.stringify(punishmentList),
        //dataType: "json",
        // error: function(resultData) {
        //     console.log(resultData);
        // },
        statusCode: {
            200: function(resultData) {
                if (resultData == "ok") {
                    //console.log(resultData);
                    window.location = "http://mrguinas.com.br/nb/control.php";
                } else {
                    $("#alert").text(resultData);
                    $("#alert").show();
                    //console.log(resultData);
                }
            },
            404: function(resultData) {
                console.log("erro");
            }
        }
    }).fail(function() {
        // console.log("error");
    });
}