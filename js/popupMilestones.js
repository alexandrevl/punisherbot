var md5Old = "";
var index = 0;
var maxIndex = 0;
$(document).ready(function() {
    updateTable();
    setInterval(function() {
        var t = updateTable();
    }, 1000);
});

document.onkeyup = function(e) {
    var e = e || window.event; // for IE to cover IEs window event-object
    if (e.which == 34) {
        nextMilestone();
        return false;
    }
    if (e.which == 33) {
        prevMilestone();
        return false;
    }
    if (e.which == 40) {
        nextMilestone();
        return false;
    }
    if (e.which == 38) {
        prevMilestone();
        return false;
    }
    if (e.which == 82) {
        resetMilestone();
        return false;
    }
}

var listMilestones = {};

function resetMilestone() {
    updateEntry(0);
}

function nextMilestone() {
    if (index < maxIndex) {
        updateEntry(++index);
    }
}

function prevMilestone() {
    if (index > 0) {
        updateEntry(--index);
    }
}

function updateEntry(i) {
    var url = "http://mrguinas.com.br/nb/updateMilestoneStatus.php?index=" + i;
    //console.log(url);
    $.ajax({
        type: 'GET',
        url: url,
        //ataType: "application/json",
        statusCode: {
            200: function(resultData) {
                //console.log(resultData);
                index = resultData;
            },
            404: function(resultData) {
                console.log("erro");
            }
        }
    }).fail(function() {
        // console.log("error");
    });
}

function updateTable() {
    var md5New = md5Old;
    var listMilestones = {};

    $.ajax({
        type: 'GET',
        url: "http://mrguinas.com.br/nb/listMilestones.php",
        dataType: "application/json",
        statusCode: {
            200: function(resultData) {
                var json = JSON.parse(resultData.responseText);
                md5New = json.md5;
                index = json.index;
                listMilestones = json.milestonesList;
                if (md5Old != md5New) {
                    maxIndex = 0;
                    $("#display").empty();
                    md5Old = md5New;
                    var text = "";
                    var i = 0;
                    listMilestones.forEach(function(milestone) {
                        ++maxIndex;
                    }, this);
                    --maxIndex;
                    listMilestones.forEach(function(milestone) {
                        if (i < index) {
                            text = '<div id="text" style="text-decoration: line-through; font-size:2vw; font-family:\'Open Sans\'; vertical-align: top; margin: 0%; padding: 0%;-webkit-text-stroke: 2px black;">' +
                                milestone.tx_milestone + ' </div>';
                        }
                        if (i == index) {
                            var percentProgress = Math.floor((i / maxIndex) * 100);
                            text = '<div id="text" style="font-size:5vw; font-family:\'Open Sans\'; vertical-align: top; margin: 0%; padding: 0%;-webkit-text-stroke: 2px black; color:yellow">' +
                                milestone.tx_milestone + ' </div>';
                            $("#progressbar").css('width', percentProgress + '%');
                            $("#progressbar").attr('class', 'progress-bar progress-bar-striped progress-bar-animated');
                            if (percentProgress > 80) {
                                $("#progressbar").attr('class', 'progress-bar progress-bar-striped progress-bar-animated bg-danger');
                            } else if (percentProgress > 70) {
                                $("#progressbar").attr('class', 'progress-bar progress-bar-striped progress-bar-animated bg-warning');
                            }
                        }
                        if (i > index) {
                            text = '<div id="text" style="font-size:3vw; font-family:\'Open Sans\'; vertical-align: top; margin: 0%; padding: 0%;-webkit-text-stroke: 2px black;">' +
                                milestone.tx_milestone + ' </div>';
                        }
                        $("#display").append(text);
                        ++i;
                    }, this);
                    //$("#text").html(text);
                }
            },
            404: function(resultData) {}
        }
    }).fail(function() {
        //console.log("error");
    });
}