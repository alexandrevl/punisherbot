<?php 
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    include 'access.inc.php'; 

    $option = 10;
    if(isset($_GET['o'])) {
        $option = $_GET['o'];
    }

    session_start();
    $db = "";

    if (!isset($_SESSION["db"])) {
        $db = new SQLite3('mysqlitedb.db');
    }
    if ($option == 0) {
        copy("mysqlitedb.db",time() . ".db");
        $result = $db->query("delete from punishment");
        $result = $db->query("delete from cooldown");
        echo "Punishments and cooldown: OK";
    }

    if ($option == 1) {
        $isPunished = false;
        $result = $db->query("select * from punishment where ts_fim > " . time(). " and ts_ini < " . time());
        while ($row = $result->fetchArray()) {
            $id= $row['id'];
            $stmt = $db->prepare("UPDATE punishment set ts_fim = :ts_fim where id = :id");
            $stmt->bindValue(':ts_fim', (time()-1), SQLITE3_INTEGER);
            $stmt->bindValue(':id', $id, SQLITE3_INTEGER);
            $result = $stmt->execute(); 
            ajustarFila($db);
            echo "Punição ativa limpa: " . $row['user'] . " - " . $row['tx_pun']; 
            $isPunished = true;
        }
        if (!$isPunished) {
            echo "Não há punição ativa";
        }
    }

    function ajustarFila($db) {
        $ts_fim_anterior = time();
        $result = $db->query("select * from punishment where ts_ini > " . time() . " order by ts_ini asc");
        while ($row = $result->fetchArray()) {
            $id = $row['id'];
            $timePunishment = ($row['ts_fim'] - $row['ts_ini'] + 1);
            $ts_ini = $ts_fim_anterior + 1;
            
            //echo " / id: " . $id . " - " . $timePunishment . " - " . $ts_ini . " - " . ($timePunishment + $ts_ini);
            
            $stmt = $db->prepare("UPDATE punishment set ts_ini = :ts_ini, ts_fim = :ts_fim where id = :id");
            $stmt->bindValue(':ts_ini', $ts_ini, SQLITE3_INTEGER);
            $stmt->bindValue(':ts_fim', ($timePunishment + $ts_ini), SQLITE3_INTEGER);
            $stmt->bindValue(':id', $id, SQLITE3_INTEGER);
            $result2 = $stmt->execute(); 
            $ts_fim_anterior = ($timePunishment + $ts_ini);
        }
    }
?>