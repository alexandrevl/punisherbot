<?php 
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Access-Control-Allow-Origin: *');

    include 'Stats.class.php';

    session_start();
    $db = "";

    if (!isset($_SESSION["db"])) {
        $db = new SQLite3('mysqlitedb.db');
    }

    $stats = new Stats();
    

    
    $result = $db->query("SELECT user, count(id) as total from punishment group by user order by total desc");
    while ($row = $result->fetchArray()) {
        $userPunishments = new UserPunishments();
        $userPunishments->user = $row['user'];
        $userPunishments->qnt = $row['total'];
        array_push($stats->usersPunishments, $userPunishments);
    }

    $result = $db->query("SELECT user, count(id) as total from cooldown group by user order by total desc");
    while ($row = $result->fetchArray()) {
        $userPunishments = new UserPunishments();
        $userPunishments->user = $row['user'];
        $userPunishments->qnt = $row['total'];
        array_push($stats->usersCooldown, $userPunishments);
    }

    $result = $db->query("SELECT tx_pun, count(id) as total from punishment group by tx_pun order by total desc");
    while ($row = $result->fetchArray()) {
        $statPun = new StatPun();
        $statPun->tx_pun = $row['tx_pun'];
        $statPun->qnt = $row['total'];
        array_push($stats->statPun, $statPun);
    }
    
    $result = $db->query("select * from punishment where ts_fim > " . time());
    while ($row = $result->fetchArray()) {
        $queue = new Queue();
        $queue->user = $row['user'];
        $queue->tx_pun = $row['tx_pun'];
        array_push($stats->queue, $queue);
    } 

    $result = $db->query("select * from config");
    while ($row = $result->fetchArray()) {
        if ($row['cd_tp'] == 1) {
            $stats->limit = $row['value'];
        }
        if ($row['cd_tp'] == 2) {
            ++$stats->qntListPunishments;
        }
    }

    $result = $db->query("SELECT count(id) as total from cooldown");
    while ($row = $result->fetchArray()) {
        $total = $row['total'];
        //echo "Total cooldown: " . $total;
        $stats->qntCooldown = $total;
    }


    $result = $db->query("SELECT count(id) as total from punishment");
    while ($row = $result->fetchArray()) {
        $total = $row['total'];
        //echo "Total punishments: " . $total;
        $stats->qntPunishments = $total;
    }

    $result = $db->query("select id from punishment where ts_fim > " . time());
    while ($row = $result->fetchArray()) {
        ++$stats->qntQueue;
    }

    $json = json_encode($stats, JSON_UNESCAPED_UNICODE);
    $stats->md5 = md5($json);
    $json = json_encode($stats, JSON_UNESCAPED_UNICODE);
    echo $json;

?>