var text = "";

var settings = {

    // Simply change the name in quotes with your name
    social: {

        // Twitch Name
        twitchUsername: "Dá follow: mr_guinas",

        // Twitter Name
        twitterUsername: "@mr_guinas",

        // Facebook Name
        facebookUsername: "MrGuinas",

        // Instagram Name
        instagramUsername: "@Mr_Guinas",

        // Youtube Name
        youtubeUsername: "Se inscreve: MrGuinas",

        // Tumblr Name
        tumblrUsername: "Dá like ae!",

        // Paypal Name
        paypalUsername: "ChangeThis",

        // Patreon Name
        patreonUsername: "ChangeThis",

        // Snapchat Name
        snapchatUsername: "ChangeThis",

        // Steam Name
        steamUsername: "ChangeThis",

        // Xbox Name
        xboxUsername: "ChangeThis",

        // Playstation Network Name
        psnUsername: "ChangeThis",

        // Origin Network Name
        originUsername: "ChangeThis",

        // Uplay Network Name
        uplayUsername: "ChangeThis",

        // Nintendo Network Name
        nintendoUsername: "ChangeThis",

        // Battle Net Network Name
        battlenetUsername: "ChangeThis",

        // Deviant Art Network Name
        deviantartUsername: "ChangeThis",

        // Reddit Network Name
        redditUsername: "ChangeThis",

        // Game Wisp Network Name
        gamewispUsername: "ChangeThis",

        // Plays.tv Network Name
        playstvUsername: "ChangeThis"

    },

    // Gaming Popup Options
    popup: {

        // This is where you enable or disable networks
        // 1 means enabled, 0 means disabled

        // Enable Twitter
        enableTwitter: 1,

        // Enable Facebook
        enableFacebook: 1,

        // Enable Instagram
        enableInstagram: 1,

        // Enable YouTube
        enableYoutube: 1,

        // Enable Tumblr
        enableTumblr: 1,

        // Enable Twitch
        enableTwitch: 1,

        // Enable PayPal
        enablePaypal: 0,

        // Enable Patreon
        enablePatreon: 0,

        // Enable Snapchat
        enableSnapchat: 0,

        // Enable Steam
        enableSteam: 0,

        // Enable Xbox
        enableXbox: 0,

        // Enable Playstation Network
        enablePsn: 0,

        // Enable Origin Network
        enableOrigin: 0,

        // Enable Uplay Network
        enableUplay: 0,

        // Enable Nintendo Network
        enableNintendo: 0,

        // Enable Battle Net Network
        enableBattleNet: 0,

        // Enable Deviant Art Network
        enableDeviantArt: 0,

        // Enable Reddit Network
        enableReddit: 0,

        // Enable Game Wisp Network
        enableGameWisp: 0,

        // Enable Plays TV Network
        enablePlaysTv: 0,


        //
        // Times to update
        //

        // Time each network animation takes in seconds
        aTime: 10,

        // The delay for the animation cycle to restart in seconds
        pauseTime: 120
    }
};

// You're all done
//
//
//
//
//
//
//
//
// No need to go any further!

// Load Social Network Names
$(".popup .right span").each(function() {
    var socialName = settings.social[$(this).data('name')];
    console.log(socialName);
    $(this).text("");
    $(this).text(socialName);
});

// Load Social Popup
$(".popup").each(function() {
    var supporterEnable = settings.popup[$(this).data('box')],
        boxName = $(this).data('box');

    if (supporterEnable == 1) {
        $('input[name=' + boxName + ']').prop('checked', true);
        $(this).addClass("animate-popup");
    } else if (supporterEnable === 0) {
        $('input[name=' + boxName + ']').prop('checked', false);
        $(this).addClass("no-popup");
    } else {
        return false;
    }
});

// Animate Popup

var popups = $('.animate-popup');
var i = 0;
var pT = settings.popup.pauseTime * 1000;

function animatePopup() {
    if (i >= popups.length) {
        i = 0;
    }
    popups.eq(i).addClass("show-popup")
        .delay(settings.popup.aTime * 1000)
        .queue(function() {
            $(this).removeClass("show-popup");
            $(this).dequeue();
            if (i == popups.length) {
                setTimeout(animatePopup, pT);
            } else {
                animatePopup();
            }
        });
    i++;
}
$(document).ready(function() {
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyBGHtVCceJzW-uQMQWNbRhjt3fSrUBxPr8",
        authDomain: "mrguinas-174619.firebaseapp.com",
        databaseURL: "https://mrguinas-174619.firebaseio.com",
        projectId: "mrguinas-174619",
        storageBucket: "mrguinas-174619.appspot.com",
        messagingSenderId: "719120864815"
    };
    firebase.initializeApp(config);
    var database = firebase.database();
    var refFirebase = database.ref('overlay/social');
    refFirebase.on('value', (data) => {
        console.log(settings);
        var popupIndex = data.val().index;
        var popupText = data.val().text;
        var popupSec = data.val().seconds;
        settings.popup.aTime = popupSec;
        switch (popupIndex) {
            case 0:
                settings.social.twitterUsername = popupText;
                break;
            case 1:
                settings.social.facebookUsername = popupText;
                break;
            case 2:
                settings.social.instagramUsername = popupText;
                break;
            case 3:
                settings.social.youtubeUsername = popupText;
                break;
            case 4:
                settings.social.tumblrUsername = popupText;
                break;
            case 5:
                settings.social.twitchUsername = popupText;
                break;

            default:
                break;
        }
        $(".popup .right span").each(function() {
            var socialName = settings.social[$(this).data('name')];
            $(this).text("");
            $(this).text(socialName);
        });
        console.log(settings.social.twitterUsername);

        popups.eq(popupIndex).addClass("show-popup")
            .delay(settings.popup.aTime * 1000)
            .queue(function() {
                $(this).removeClass("show-popup");
                $(this).dequeue();
            });
    });


});
//animatePopup();