A Pen created at CodePen.io. You can find this one at https://codepen.io/NerdOrDie/pen/vNEvee.

 A simple HTML based popup that will display social networks.

If you're coming here from CodePen's front page and wondering what this is, I made it a while back to allow streamers (on Twitch, YouTube, etc.) to show their social networks during their broadcast.

Myself and my team make quite a few free resources for streamers, so if you're interested - check them out here: https://nerdordie.com/product-tag/free-resource/