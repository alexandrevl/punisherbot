<?php  
include 'access.inc.php'; 
?>
<?php
if(isset($_GET['v'])) {
        $v = $_GET['v'];
    }
$isColor = true;
if(isset($_GET['c'])) {
        $isColor = false;
    } 
?>

<html>

<head>
    <title>
        MrGuinas - Dashboard (Com Color)
    </title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="img/Logo.png" />
    <style>
    .foo {
        float: left;
        width: 20px;
        height: 20px;
        margin: 5px;
        border: 1px solid rgba(0, 0, 0, .2);
    }

    .blue {
        background: #0000FF;
    }

    .purple {
        background: #800080;
    }

    .red {
        background: #FF0000;
    }

    .green {
        background: #008000;
    }

    .orange {
        background: #FFA500;
    }

    .pink {
        background: #FFC0CB;
    }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
        integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous">
    </script>
    <script src="https://www.gstatic.com/firebasejs/5.4.0/firebase.js"></script>
    <script src="js/popout.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
        integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>

<body style="margin: 0;padding: 0;background: #000000;height: 100%; overflow: hidden">
    <div class="modal fade" id="color" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Mudar a cor do quarto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="cores">
                    <center>
                        <a href="#">
                            <div class="foo blue" id="blue"></div>
                        </a>
                        <a href="#">
                            <div class="foo purple" id="purple"></div>
                        </a>
                        <a href="#">
                            <div class="foo red" id="red"></div>
                        </a>
                        <a href="#">
                            <div class="foo pink" id="pink"></div>
                        </a>
                        <a href="#">
                            <div class="foo orange" id="orange"></div>
                        </a>
                        <a href="#">
                            <div class="foo green" id="green"></div>
                        </a>
                        <button type="button" class="btn btn-outline-primary btn-sm" id="rainbow">rainbow</button>
                    </center>

                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="OkResetar" data-dismiss="modal">Sim, exclui tudo</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não, fecha isso</button>
                </div> -->
            </div>
        </div>
    </div>
    <div class="modal fade" id="titleDialog" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Define Meta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="restreamTitle">
                    <iframe src="https://dashboard.mrguinas.com.br/changeMeta.php" height="500" width="460"
                        style="border:none;background: #B8B8B8;" scrolling="yes" id="meta"></iframe>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="OkResetar" data-dismiss="modal">Sim, exclui tudo</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Não, fecha isso</button>
                </div> -->
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-2 text-center">
            <iframe src="https://streamlabs.com/dashboard/recent-events" height="615" width="235" style="border:none;"
                scrolling="no"></iframe>
            <br>
            <center>
                <button type="button" class="btn btn-outline-success btn-sm" id="color" data-toggle="modal"
                    data-target="#color"><i class="far fa-lightbulb"></i></button>
                <button type="button" class="btn btn-outline-warning btn-sm" id="title" data-toggle="modal"
                    data-target="#titleDialog"><i class="fas fa-pen"></i></button>
                <button type="button" class="btn btn-outline-primary btn-sm" id="like"><i
                        class="far fa-thumbs-up"></i></button>
                <button type="button" class="btn btn-outline-primary btn-sm" id="youtube"><i
                        class="fab fa-youtube"></i></button>
                <!-- <button type="button" class="btn btn-outline-primary btn-sm" id="twitter"><i class="fab fa-twitter"></i></button> -->
                <button type="button" class="btn btn-outline-primary btn-sm" id="twitch"><i
                        class="fab fa-twitch"></i></button>
            </center>
        </div>
        <div class="col-4 text-center">
            <iframe src="https://www.youtube.com/live_chat?is_popout=1&v=nSm6h1EbMis" height="645" width="400"
                style="border:none;" scrolling="no"></iframe>
        </div>
        <div class="col-4 text-center">
            <iframe src="https://mrguinas.com.br/blab/blab.php" height="645" width="420"
                style="border:none;background: #B8B8B8;" scrolling="yes"></iframe>
            <!-- <iframe src="https://www.twitch.tv/popout/mr_guinas/chat?popout=" height="445" width="460"
                style="border:none;" scrolling="no"></iframe> -->
        </div>
        <div class="col-2 text-left">
            <iframe id="counter"
                src="https://dashboard.mrguinas.com.br/counter/?twitch=mr_guinas&youtube=UCCFcBOBr7B3ob4JjyTRdTRQ&v=0&vy=400&vt=Total%20Viewers%3A&vtfs=20&vnfs=20&vnfw=bold&viy=0&vitfs=30&vinfs=40&vinfw=bold&f=0&fx=0&fy=260&fix=0&fiy=160&finfw=bold&l=1&ltfs=17&ltc=ff2600&lnfs=26&lnfw=bold"
                height="200" width="330" style="border:none;" scrolling="no"></iframe>
            <!-- <iframe id="counter" src="https://loots.com/en/account/tips/condensed" height="445" width="330"
                style="border:none;" scrolling="yes"></iframe> -->
            <!-- <iframe id="counter" src="https://color.mrguinas.com.br/" height="0" width="-" style="border:none;"
                scrolling="no"></iframe> -->
        </div>
    </div>

</body>

</html>