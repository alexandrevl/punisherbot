<?php
header('Content-Type: text/html; charset=utf-8');  
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Access-Control-Allow-Origin: *');

include 'cred.php';
include 'channels.php';
static $key = 'AIzaSyBb-V4jY3SSBfbq7_5kI31iFBPXTW2sWnw';


foreach ($channelIdArray as $key2 => $value) {
    echo $key2 . ": ";
    getVideoIdOptions($value);
    echo "\n";
}

function getVideoIdOptions($channelId)
{
    global $key;
    $videoId = '';
    $getVideoIdOptions = 'https://www.googleapis.com/youtube/v3/search?part=snippet&eventType=live&type=video&maxResults=50&channelId=' . $channelId . '&key=' . $key;
    $response = file_get_contents($getVideoIdOptions);
    $videoOptions = json_decode($response);
    
    if (count($videoOptions->items)) {
        $dados = new stdClass(); 
        $videoId = $videoOptions->items[0]->id->videoId;
        $dados->videoOptions = new stdClass();;
        $dados->videoOptions->id = $videoId;
        $dados->videoOptions->title = $videoOptions->items[0]->snippet->title;
        $dados->videoOptions->channelId = $videoOptions->items[0]->snippet->channelId;
        $dados->videoOptions->publishedAt = $videoOptions->items[0]->snippet->publishedAt;
        $dados->videoOptions->channelTitle = $videoOptions->items[0]->snippet->channelTitle;

        $dados->viewerOptions = getViewerOptions($videoId);
        $dados->followerOptions = getFollowerOptions($channelId);
        //echo json_encode($dados);
        setStats($dados);
    } else {
        echo "off";
    }
}

function getViewerOptions($videoId)
{
    global $key;
    $getViewerOptions = 'https://www.googleapis.com/youtube/v3/videos?part=liveStreamingDetails,statistics&id=' . $videoId . '&key=' . $key;
    $response = file_get_contents($getViewerOptions);
    $viewerOptions = json_decode($response);

    $dados = new stdClass();
    $dados->statistics = $viewerOptions->items[0]->statistics;
    $dados->liveStreamingDetails = $viewerOptions->items[0]->liveStreamingDetails;
    return $dados;
   
}
function getFollowerOptions($channelId)
{
    global $key;
    $getFollowerOptions = 'https://www.googleapis.com/youtube/v3/channels?part=statistics&id=' . $channelId . '&key=' . $key;
    $response = file_get_contents($getFollowerOptions);
    $followerOptions = json_decode($response);
    $dados = new stdClass();
    $dados->statistics = $followerOptions->items[0]->statistics; 
    return $dados;
}
function setStats($dados)
{
    global $dsn;
    $dados->timestamp = time();
    $conn = new PDO($dsn);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if ($conn) {
        $id = 1;
        $data = [
            'dados' => json_encode($dados),
        ];
        try {
            $sql = 'INSERT INTO streamstats ("dados") VALUES (:dados)';
            $stmt2 = $conn->prepare($sql);
            $result = $stmt2->execute($data);
            echo 'on';
        } catch (PDOException $e2) {
            echo 'Error: ' . $e2->getMessage();
        }
    }
}
