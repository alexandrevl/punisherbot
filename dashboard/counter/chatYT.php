<?php

header('Content-Type: text/html; charset=utf-8');  
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Access-Control-Allow-Origin: *');

ini_set('session.gc_maxlifetime', 3600 * 10);
session_set_cookie_params(3600 * 10);
session_start();

static $key = 'AIzaSyBb-V4jY3SSBfbq7_5kI31iFBPXTW2sWnw';
static $channelId = 'UCCFcBOBr7B3ob4JjyTRdTRQ';

main();

function main()
{
    if (getLiveChatId()) {
        getMessages();
    } else {
        echo "off";
    }
}

function getLiveChatId()
{
    global $key, $channelId;
    if (!isset($_SESSION['liveChatId'])) {
        $getVideoIdOptions = 'https://www.googleapis.com/youtube/v3/search?part=snippet&eventType=live&type=video&maxResults=50&channelId=' . $channelId . '&key=' . $key;
        $response = file_get_contents($getVideoIdOptions);
        $videoOptions = json_decode($response);

        if (count($videoOptions->items)) {
            $videoId = $videoOptions->items[0]->id->videoId;

            $getViewerOptions = 'https://www.googleapis.com/youtube/v3/videos?part=liveStreamingDetails,statistics&id=' . $videoId . '&key=' . $key;
            $response = file_get_contents($getViewerOptions);
            $viewerOptions = json_decode($response);
            $liveChatId = $viewerOptions->items[0]->liveStreamingDetails->activeLiveChatId;
            $_SESSION['liveChatId'] = $liveChatId;
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

function getMessages()
{
    global $key, $channelId;
    $liveChatId = $_SESSION['liveChatId'];
    try {
        $listMessages = 'https://www.googleapis.com/youtube/v3/liveChat/messages?liveChatId=' . $liveChatId . '&part=snippet&key=' . $key;
        if(get_http_response_code($listMessages) != "200"){
            unset($_SESSION['liveChatId']);
        echo "off";
        } else {
            $response = file_get_contents($listMessages);
            $chatMessages = json_decode($response);
            //echo $chatMessages->items;
            foreach ($chatMessages->items as $key => $msg) {
                echo $msg->snippet->displayMessage . '<br>';
            }
        }
    } catch (Exception $e) {
        unset($_SESSION['liveChatId']);
        echo "off";
    }

}
function get_http_response_code($url) {
    $headers = get_headers($url);
    return substr($headers[0], 9, 3);
}
