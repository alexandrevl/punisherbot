<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,PATCH,OPTIONS');
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');


include 'cred.php';

$channel = null;
 
if(isset($_GET['c'])) {
    $channel = $_GET['c']; 
}


    $conn = new PDO($dsn);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if ($conn) {
        try {
            $sql = "SELECT dados FROM streamstats order by id";
            if ($channel != null) {
                $sql = "SELECT dados FROM streamstats where dados->'videoOptions'->>'channelId' = '" .  $channel . "' order by id";
            }
            $stmt = $conn->query($sql);
            $resultado = Array();
            while ($row = $stmt->fetch()) {
                $dados = new stdClass();
                $r = json_decode($row['dados']);
                $dados->id = $r->videoOptions->id;
                $dados->title = $r->videoOptions->title;
                $resultado[$dados->id] = $dados->title;
                // array_push($resultado,$dados);
            }
            echo json_encode($resultado);

        } catch (PDOException $e2) {
            echo 'Error: ' . $e2->getMessage();
        }
    }