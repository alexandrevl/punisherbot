<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,PATCH,OPTIONS');
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');
 

include 'channels.php'; 

echo json_encode($channelIdArray);