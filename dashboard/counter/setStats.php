<?php
$http_origin = $_SERVER['HTTP_ORIGIN'];
if (strpos($http_origin, "mrguinas.com.br") !== false ) {  
    header("Access-Control-Allow-Origin: $http_origin");
}
header("Access-Control-Allow-Headers: Content-Type");
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');
include '../cred.php';

//Make sure that it is a POST request.
if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0) {
    throw new Exception('Request method must be POST!');
}

//Make sure that the content type of the POST request has been set to application/json
$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
if (strcasecmp($contentType, 'application/json') != 0) {
    throw new Exception('Content type must be: application/json');
}
//Receive the RAW post data.
$content = trim(file_get_contents("php://input"));

//Attempt to decode the incoming RAW post data from JSON.
$decoded = json_decode($content, true);

//If json_decode failed, the JSON is invalid.
if (!is_array($decoded)) {
    throw new Exception('Received content contained invalid JSON!');
}

$conn = new PDO($dsn);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if ($conn) {
    $id = 1;
    $data = [
        'dados' => json_encode($decoded),
    ];
    try {
        $sql = 'INSERT INTO livestats ("dados") VALUES (:dados)';
        $stmt2 = $conn->prepare($sql);
        $result = $stmt2->execute($data);
        //print_r($stmt2->fetch());
        echo json_encode($decoded);
    } catch (PDOException $e2) {
        echo 'Error: ' . $e2->getMessage();
    }

}
