<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,PATCH,OPTIONS');
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');


include 'cred.php';

$channel = null;
$video = null;

if(isset($_GET['c'])) {
    $channel = $_GET['c']; 
}
if(isset($_GET['v'])) {
    $video = $_GET['v']; 
}


    $conn = new PDO($dsn);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if ($conn) {
        try {
            $sql = "SELECT dados FROM streamstats order by id";
            if ($channel != null) {
                $sql = "SELECT dados FROM streamstats where dados->'videoOptions'->>'channelId' = '" .  $channel . "' order by id";
            }
            if ($video != null) {
                $sql = "SELECT dados FROM streamstats where dados->'videoOptions'->>'id' = '" .  $video . "' order by id";
            }
            $stmt = $conn->query($sql);
            $resultado = Array();
            while ($row = $stmt->fetch()) {
                array_push($resultado,$row["dados"]);
            }
            echo json_encode($resultado);

        } catch (PDOException $e2) {
            echo 'Error: ' . $e2->getMessage();
        }
    }