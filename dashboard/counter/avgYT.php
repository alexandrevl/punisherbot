<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE,PATCH,OPTIONS');
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');


include 'cred.php';

$channel = null;
$video = null;

if(isset($_GET['c'])) {
    $channel = $_GET['c']; 
}
if(isset($_GET['v'])) {
    $video = $_GET['v']; 
}


    $conn = new PDO($dsn);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    if ($conn) {
        try {
            $videoId = null;
            $sql = "SELECT dados FROM streamstats order by id";
            if ($channel != null) {
                $sql = "SELECT dados FROM streamstats where dados->'videoOptions'->>'channelId' = '" .  $channel . "' order by id";
            }
            if ($video != null) {
                $sql = "SELECT dados FROM streamstats where dados->'videoOptions'->>'id' = '" .  $video . "' order by id";
            }
            $data = Array();
            $data['count'] = 0;
            $stmt = $conn->query($sql);
            $resultado = Array();
            while ($row = $stmt->fetch()) {
                $dados = json_decode($row["dados"],true);
                if ($videoId != $dados["videoOptions"]["id"]) {
                    if ($videoId != null) {
                        $avg = Array();
                        $avg["videoId"] = $videoId;
                        $avg["videoTitle"] = $data["videoTitle"];
                        $avg["actualStartTime"] = $data["actualStartTime"];
                        $avg['avgViewers'] = $data["sumViewers"]/$data["count"];
                        $avg['avglikes'] = $data["sumLikes"]/$data["count"];
                        $avg['avgConcurrentViewers'] = $data["sumConcurrentViewers"]/$data["count"];
                        array_push($resultado,$avg);
                        $data = Array();
                        $data['count'] = 0;
                    }
                    $videoId = $dados["videoOptions"]["id"];
                    //echo $videoId;
                    //echo "---------------";
                }
                $data["sumViewers"] += $dados['viewerOptions']['statistics']['viewCount'];
                $data["sumLikes"] += $dados['viewerOptions']['statistics']['likeCount'];
                $data["sumConcurrentViewers"] += $dados['viewerOptions']['liveStreamingDetails']['concurrentViewers'];
                $data["actualStartTime"] = $dados['viewerOptions']['liveStreamingDetails']['actualStartTime'];
                $data["count"]++;
                $data["videoId"] = $videoId;
                $data["videoTitle"] = $dados['videoOptions']['title'];
                //print_r($dados);
                //array_push($resultado,$row["dados"]);
            }
            if ($videoId != null) {
                $avg = Array();
                $avg["videoId"] = $videoId;
                $avg["videoTitle"] = $data["videoTitle"];
                $avg["actualStartTime"] = $data["actualStartTime"];
                $avg['avgViewers'] = $data["sumViewers"]/$data["count"];
                $avg['avglikes'] = $data["sumLikes"]/$data["count"];
                $avg['avgConcurrentViewers'] = $data["sumConcurrentViewers"]/$data["count"];
                array_push($resultado,$avg);
                $data = Array();
                $data['count'] = 0;
            }
            echo json_encode($resultado);

        } catch (PDOException $e2) {
            echo 'Error: ' . $e2->getMessage();
        }
    }