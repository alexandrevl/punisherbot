var platform = function(name) {
	this.name = name;
	this.channel = null;
	this.client_id = null;
	this.access_token = null;
	this.viewers = 0;
	this.followers = 0;
};

var dados = {};

var twitch = new platform("twitch");
var youtube = new platform("youtube");
var beam = new platform("beam");
var hitbox = new platform("hitbox");

var platform_names = ["twitch", "youtube", "beam", "hitbox"];

function nullFunction() {}

twitch.init = function() {
	var self = this;

	self.getViewerFollowerOptions = {
		url: "https://api.twitch.tv/kraken/streams/" + self.channel,
		headers: { "Client-ID": self.client_id }
	};

	self.getFollowerOptions = {
		url: "https://api.twitch.tv/kraken/channels/" + self.channel + "/follows?limit=1",
		headers: { "Client-ID": self.client_id }
	};

	self.updateCounter = self.channel
		? function() {
				$.ajax(self.getViewerFollowerOptions)
					.done(function(data) {
						if (data.hasOwnProperty("stream") && data.stream !== null) {
							hideCounter(self.name);
							dados.stream = data.stream;
							showCounterByType(viewer_txt, self.name, data.stream.viewers);

							if (data.stream.hasOwnProperty("channel")) showCounterByType(follower_txt, self.name, data.stream.channel.followers);
						} else {
							hideCounterByType(viewer_txt, self.name);

							if (options.isVisibleFollower()) {
								$.ajax(self.getFollowerOptions)
									.done(function(data) {
										if (data && data.hasOwnProperty("_total")) showCounterByType(follower_txt, self.name, data._total);
										else hideCounterByType(follower_txt, self.name);
									})
									.fail(function() {
										hideCounterByType(follower_txt, self.name);
									});
							}
						}
					})
					.fail(function() {
						hideCounter(self.name);
					});
		  }
		: nullFunction;
};

youtube.init = function() {
	var self = this;

	self.search_title = getParameter("youtube_title");
	if (self.search_title === "") self.search_title = null;

	self.video_id = "";

	self.getVideoIdOptions =
		"https://www.googleapis.com/youtube/v3/search?part=snippet&eventType=live&type=video&maxResults=" + (self.search_title === null ? "1" : "50") + "&channelId=" + self.channel + "&key=";
	self.getViewerOptions = "https://www.googleapis.com/youtube/v3/videos?part=liveStreamingDetails";
	self.getFollowerOptions = "https://www.googleapis.com/youtube/v3/channels?part=statistics&id=" + self.channel + "&key=";
	self.getLikesOptions = ",statistics";

	self.getVideoId_timeout = false;

	self.isVideoMatch = function(item) {
		if (item.id && item.id.videoId) {
			self.video_id = item.id.videoId;
			self.getViewer();
			return true;
		}
		return false;
	};

	self.hideViewerCounter = function() {
		self.video_id = "";
		hideCounterByType(viewer_txt, self.name);
		hideLikeCounter(self.name);
	};

	self.getVideoId = function(get_viewer) {
		self.video_id = "";

		if (self.getVideoId_timeout === false) {
			self.getVideoId_timeout = setTimeout(function() {
				self.getVideoId_timeout = false;
			}, 60000);

			$.getJSON(self.getVideoIdOptions + getGoogleApiKey(), function(data) {
				//console.log("VideoID", data);
				if (data && data.items && data.items.length) {
					dados.snippet = data.items[0].snippet;
					if (self.search_title !== null) {
						var i = 0;
						for (; i < data.items.length; ++i) {
							var item = data.items[i];
							if (item.snippet && item.snippet.title && item.snippet.title === self.search_title && self.isVideoMatch(item)) break;
						}
						if (i === data.items.length) self.hideViewerCounter();
					} else {
						if (!self.isVideoMatch(data.items[0])) self.hideViewerCounter();
					}
				} else {
					self.hideViewerCounter();
					hideLikeCounter(self.name);
				}
			});
		}
	};

	self.getViewer = function() {
		//console.log("x", self.video_id);
		if (self.video_id !== "") {
			$.getJSON(self.getViewerOptions + (options.isVisibleLike() ? self.getLikesOptions : "") + "&id=" + self.video_id + "&key=" + getGoogleApiKey(), function(data) {
				console.log(data);
				hideCounterByType(viewer_txt, self.name);
				hideLikeCounter(self.name);
				if (data && data.items && data.items[0].liveStreamingDetails && !data.items[0].liveStreamingDetails.hasOwnProperty("actualEndTime")) {
					dados.liveStreamingDetails = data.items[0].liveStreamingDetails;
					dados.statistics = data.items[0].statistics;
					//console.log(data.items[0].liveStreamingDetails);
					//console.log(data.items[0].statistics);
					if (options.isVisibleViewer()) showCounterByType(viewer_txt, self.name, data.items[0].liveStreamingDetails.concurrentViewers || 0);
					if (options.isVisibleLike()) showLikeCounter(self.name, data.items[0].statistics.likeCount || 0, data.items[0].statistics.dislikeCount || 0);
				} else self.getVideoId();
			});
		} else self.getVideoId();
	};

	self.updateCounter = self.channel
		? function() {
				if (options.isVisibleViewer() || options.isVisibleLike()) self.getViewer();

				if (options.isVisibleFollower()) {
					$.getJSON(self.getFollowerOptions + getGoogleApiKey(), function(data) {
						if (data && data.items && data.items.length && data.items[0].statistics) {
							showCounterByType(follower_txt, self.name, data.items[0].statistics.subscriberCount);
						} else {
							hideCounterByType(follower_txt, self.name);
						}
					}).fail(function() {
						hideCounterByType(follower_txt, self.name);
					});
				}
		  }
		: nullFunction;
};

$(document).ready(function() {
	var url_string = window.location.href;
	var url = new URL(url_string);
	var c = url.searchParams.get("isNoColor");

	// if (c != 1) {
	//     console.log("Interval On");
	//     setInterval(function() {
	//         setStats();
	//     }, 60000);
	// } else {
	//     console.log("Interval Off");
	// }
});

function setStats() {
	console.log("Set stats...");
	if (dados.liveStreamingDetails || dados.stream) {
		dados.timestamp = new Date().getTime();
		var jsonPost = dados;
		$.ajax({
			type: "POST",
			url: "https://dashboard.mrguinas.com.br/counter/setStats.php",
			contentType: "application/json",
			dataType: "json",
			data: JSON.stringify(jsonPost),
			cache: false,
			statusCode: {
				200: function(response) {
					console.log(response);
				},
				404: function(resultData) {
					console.log(resultData);
				}
			}
		});
	} else {
		console.log("Não tem o que enviar", dados);
	}
}

beam.init = function() {
	var self = this;

	self.getViewerFollowerOptions = "https://mixer.com/api/v1/channels/" + self.channel + "?fields=viewersCurrent,numFollowers,online";

	self.updateCounter = self.channel
		? function() {
				$.getJSON(self.getViewerFollowerOptions, function(data) {
					hideCounter(self.name);
					if (data) {
						if (data.online) showCounterByType(viewer_txt, self.name, data.viewersCurrent);
						else hideCounterByType(viewer_txt, self.name);

						showCounterByType(follower_txt, self.name, data.numFollowers);
					}
				}).fail(function() {
					hideCounter(self.name);
				});
		  }
		: nullFunction;
};

hitbox.init = function() {
	var self = this;

	self.getViewerFollowerOptions = "https://api.smashcast.tv/media/live/" + self.channel;

	self.updateCounter = self.channel
		? function() {
				$.getJSON(self.getViewerFollowerOptions, function(data) {
					hideCounter(self.name);
					if (data && data.livestream && data.livestream.length) {
						if (data.livestream[0].media_is_live !== "0") showCounterByType(viewer_txt, self.name, data.livestream[0].media_views);
						else hideCounterByType(viewer_txt, self.name);

						if (data.livestream[0].channel) showCounterByType(follower_txt, self.name, data.livestream[0].channel.followers);
					}
				}).fail(function() {
					hideCounter(self.name);
				});
		  }
		: nullFunction;
};

var viewer_txt = "viewer";
var follower_txt = "follower";

var display_object = function(
	visible,
	x,
	y,
	title,
	title_font_family,
	title_custom_font_family,
	title_font_size,
	title_font_weight,
	title_italic,
	title_underline,
	title_color,
	title_opacity,
	title_outline_color,
	title_outline_opacity,
	title_outline_width,
	num_font_family,
	num_custom_font_family,
	num_font_size,
	num_font_weight,
	num_italic,
	num_underline,
	num_color,
	num_opacity,
	num_outline_color,
	num_outline_opacity,
	num_outline_width
) {
	this.setValue = function(
		visible,
		x,
		y,
		title,
		title_font_family,
		title_custom_font_family,
		title_font_size,
		title_font_weight,
		title_italic,
		title_underline,
		title_color,
		title_opacity,
		title_outline_color,
		title_outline_opacity,
		title_outline_width,
		num_font_family,
		num_custom_font_family,
		num_font_size,
		num_font_weight,
		num_italic,
		num_underline,
		num_color,
		num_opacity,
		num_outline_color,
		num_outline_opacity,
		num_outline_width
	) {
		this.visible = visible;
		this.x = x;
		this.y = y;
		this.title = title;
		this.title_font_family = title_font_family;
		this.title_custom_font_family = title_custom_font_family;
		this.title_font_size = title_font_size;
		this.title_font_weight = title_font_weight;
		this.title_italic = title_italic;
		this.title_underline = title_underline;
		this.title_color = title_color;
		this.title_opacity = title_opacity;
		this.title_outline_color = title_outline_color;
		this.title_outline_opacity = title_outline_opacity;
		this.title_outline_width = title_outline_width;
		this.num_font_family = num_font_family;
		this.num_custom_font_family = num_custom_font_family;
		this.num_font_size = num_font_size;
		this.num_font_weight = num_font_weight;
		this.num_italic = num_italic;
		this.num_underline = num_underline;
		this.num_color = num_color;
		this.num_opacity = num_opacity;
		this.num_outline_color = num_outline_color;
		this.num_outline_opacity = num_outline_opacity;
		this.num_outline_width = num_outline_width;
	};

	this.setValue(
		visible,
		x,
		y,
		title,
		title_font_family,
		title_custom_font_family,
		title_font_size,
		title_font_weight,
		title_italic,
		title_underline,
		title_color,
		title_opacity,
		title_outline_color,
		title_outline_opacity,
		title_outline_width,
		num_font_family,
		num_custom_font_family,
		num_font_size,
		num_font_weight,
		num_italic,
		num_underline,
		num_color,
		num_opacity,
		num_outline_color,
		num_outline_opacity,
		num_outline_width
	);
};

var options = {
	viewer: new display_object(
		true,
		"0",
		"0",
		"Viewers:",
		"Open Sans",
		"",
		"30",
		"normal",
		false,
		false,
		"ffffff",
		"1",
		"000000",
		"1",
		"1",
		"Open Sans",
		"",
		"30",
		"normal",
		false,
		false,
		"ffffff",
		"1",
		"000000",
		"1",
		"1"
	),
	viewer_i: new display_object(
		true,
		"0",
		"40",
		"",
		"Open Sans",
		"",
		"20",
		"normal",
		false,
		false,
		"ffffff",
		"1",
		"000000",
		"1",
		"1",
		"Open Sans",
		"",
		"20",
		"normal",
		false,
		false,
		"ffffff",
		"1",
		"000000",
		"1",
		"1"
	),
	follower: new display_object(
		true,
		"500",
		"0",
		"Followers:",
		"Open Sans",
		"",
		"30",
		"normal",
		false,
		false,
		"ffffff",
		"1",
		"000000",
		"1",
		"1",
		"Open Sans",
		"",
		"30",
		"normal",
		false,
		false,
		"ffffff",
		"1",
		"000000",
		"1",
		"1"
	),
	follower_i: new display_object(
		true,
		"500",
		"40",
		"",
		"Open Sans",
		"",
		"20",
		"normal",
		false,
		false,
		"ffffff",
		"1",
		"000000",
		"1",
		"1",
		"Open Sans",
		"",
		"20",
		"normal",
		false,
		false,
		"ffffff",
		"1",
		"000000",
		"1",
		"1"
	),

	like: new display_object(
		false,
		"0",
		"80",
		"",
		"Open Sans",
		"",
		"20",
		"normal",
		false,
		false,
		"0080c0",
		"1",
		"000000",
		"1",
		"0",
		"Open Sans",
		"",
		"20",
		"normal",
		false,
		false,
		"ffffff",
		"1",
		"000000",
		"1",
		"1"
	),

	isVisibleViewer: function() {
		return this.viewer.visible || this.viewer_i.visible;
	},
	isVisibleFollower: function() {
		return this.follower.visible || this.follower_i.visible;
	},
	isVisibleLike: function() {
		return this.like.visible;
	}
};

var default_options = JSON.parse(JSON.stringify(options));

var parameter_names = {
	visible: "",
	x: "x",
	y: "y",

	title: "t",
	title_font_family: "tf",
	title_custom_font_family: "tcf",
	title_font_size: "tfs",
	title_font_weight: "tfw",
	title_italic: "ti",
	title_underline: "tu",
	title_color: "tc",
	title_opacity: "to",
	title_outline_color: "toc",
	title_outline_opacity: "too",
	title_outline_width: "tow",

	num_font_family: "nf",
	num_custom_font_family: "ncf",
	num_font_size: "nfs",
	num_font_weight: "nfw",
	num_italic: "ni",
	num_underline: "nu",
	num_color: "nc",
	num_opacity: "no",
	num_outline_color: "noc",
	num_outline_opacity: "noo",
	num_outline_width: "now"
};

var svg_bbox_interval = null;
var svg_bbox_interval_count = 0;

var escape_HTML_container = $("<textarea></textarea>");

function getGoogleApiKey() {
	return google_api_key[Math.floor(Math.random() * google_api_key.length)];
}

function setText(svg_obj, text) {
	if (typeof text !== "undefined") {
		var text_obj = svg_obj.children("text");
		escape_HTML_container.text(text);
		text_obj.html(escape_HTML_container.html().replace(/ /g, "&nbsp;"));
	}

	resizeSvg(svg_obj);
}

function resizeSvg(svg_obj) {
	if (!svg_obj.is(":visible")) return;

	var stroke_width = parseInt(svg_obj.css("stroke-width"));
	if (isNaN(stroke_width)) stroke_width = 0;

	var child = svg_obj.children("text");

	var box = child[0].getBBox();

	var offset = box.width !== 0 ? stroke_width + 2 : 0;
	var x = stroke_width / 2 + (box.x < 0 ? box.x * -1 : 0);
	var y = stroke_width / 2 + (box.y < 0 ? box.y * -1 : 0);

	svg_obj.css({ width: box.width + offset + "px", height: box.height + offset + "px" });
	child.css("transform", "translate(" + x + "px," + y + "px)");
}

var getMaxSize = function() {};

function resizeAllSvg() {
	$("svg").each(function() {
		resizeSvg($(this));
	});

	getMaxSize();
}

function showCounterByType(type, platform, num) {
	if (!$.isNumeric(num)) num = 0;

	window[platform][type + "s"] = Number(num);
	updateTotalCounterByType(type);

	$("#" + type + "-div-" + platform).removeClass("hide");
	setText($("#" + type + "-counter-" + platform), window[platform][type + "s"]);
}

function updateTotalCounterByType(type) {
	var total = 0;
	for (var i = 0; i < platform_names.length; ++i) {
		var platform = window[platform_names[i]];
		if (platform.channel) total += platform[type + "s"];
	}

	setText($("#" + type + "-counter-total"), total);
}

function hideCounter(platform) {
	$("#viewer-div-" + platform + ", #follower-div-" + platform).addClass("hide");
	hideLikeCounter(platform);
}

function hideCounterByType(type, platform) {
	$("#" + type + "-div-" + platform).addClass("hide");
}

function showLikeCounter(platform, likes, dislikes) {
	if (platform !== "youtube") return;

	if (!$.isNumeric(likes)) likes = 0;
	if (!$.isNumeric(dislikes)) dislikes = 0;

	$("#like-div-" + platform).removeClass("hide");
	setText($("#like-counter-" + platform), likes);
	$("#dislike-div-" + platform).removeClass("hide");
	setText($("#dislike-counter-" + platform), dislikes);
}

function hideLikeCounter(platform) {
	if (platform !== "youtube") return;

	$("#like-div-" + platform).addClass("hide");
	$("#dislike-div-" + platform).addClass("hide");
}

function updateCounter() {
	for (var i = 0; i < platform_names.length; ++i) window[platform_names[i]].updateCounter();
}

function setFont(id, font_family, custom_font, font_size, font_weight, italic, underline, color, outline_color, outline_width) {
	if (custom_font !== "") {
		font_family = custom_font + ",sans-serif";
		custom_font = true;
	} else custom_font = false;

	if (font_family === "" || font_family === null) {
		font_family = "sans-serif";
		custom_font = true;
	}

	if (!custom_font) {
		var font_ele = document.getElementById(id + "-font-link");
		if (font_ele === null) {
			font_ele = document.createElement("link");
			font_ele.id = id + "-font-link";
			font_ele.rel = "stylesheet";
			font_ele.type = "text/css";
			document.head.appendChild(font_ele);
		}

		var href = "https://fonts.googleapis.com/css?family=" + font_family.replace(/ /g, "+");
		if (font_ele.href !== href) font_ele.href = href;
	}

	var ele = $("." + id);
	ele.css({
		fontFamily: font_family,
		fontSize: $.isNumeric(font_size) ? font_size + "px" : "",
		fill: "#" + color,
		stroke: "#" + outline_color,
		fontWeight: font_weight,
		//		fontStyle: (italic ? 'italic' : 'normal'),
		transform: italic ? "skewX(160deg)" : "none",
		textDecoration: underline ? "underline" : "none",
		strokeWidth: $.isNumeric(outline_width) ? outline_width * 2 + "px" : ""
	});
}

function getParameter(val) {
	var regex = new RegExp("[\\?&#]" + val + "=([^&#]*)");
	var para = regex.exec(document.location.search);

	return para ? decodeURIComponent(para[1]) : null;
}

function getChannelId(platform) {
	return setChannelId(platform, getParameter(platform));
}

function setBoolByParameter(obj, prefix, item) {
	var value = getParameter(prefix + parameter_names[item]);
	if (value !== null) obj[item] = value !== "0";
}

function setValueByParameter(obj, prefix, item) {
	var value = getParameter(prefix + parameter_names[item]);
	if (value !== null) obj[item] = value;
}

function getOptionByType(type, prefix) {
	var obj = options[type];

	setBoolByParameter(obj, prefix, "visible");
	setValueByParameter(obj, prefix, "x");
	setValueByParameter(obj, prefix, "y");

	setValueByParameter(obj, prefix, "title");
	setValueByParameter(obj, prefix, "title_font_family");
	setValueByParameter(obj, prefix, "title_custom_font_family");
	setValueByParameter(obj, prefix, "title_font_size");
	setValueByParameter(obj, prefix, "title_font_weight");
	setValueByParameter(obj, prefix, "title_italic");
	setValueByParameter(obj, prefix, "title_underline");
	setValueByParameter(obj, prefix, "title_color");
	setValueByParameter(obj, prefix, "title_opacity");
	setValueByParameter(obj, prefix, "title_outline_color");
	setValueByParameter(obj, prefix, "title_outline_opacity");
	setValueByParameter(obj, prefix, "title_outline_width");

	setValueByParameter(obj, prefix, "num_font_family");
	setValueByParameter(obj, prefix, "num_custom_font_family");
	setValueByParameter(obj, prefix, "num_font_size");
	setValueByParameter(obj, prefix, "num_font_weight");
	setValueByParameter(obj, prefix, "num_italic");
	setValueByParameter(obj, prefix, "num_underline");
	setValueByParameter(obj, prefix, "num_color");
	setValueByParameter(obj, prefix, "num_opacity");
	setValueByParameter(obj, prefix, "num_outline_color");
	setValueByParameter(obj, prefix, "num_outline_opacity");
	setValueByParameter(obj, prefix, "num_outline_width");
}

function getOptions() {
	getOptionByType("viewer", "v");
	getOptionByType("viewer_i", "vi");
	getOptionByType("follower", "f");
	getOptionByType("follower_i", "fi");
	getOptionByType("like", "l");

	applyOptions();
}

function setChannelId(platform, id) {
	if (id === "") id = null;
	window[platform].channel = id;

	if (id === null) hideCounter(platform);

	return id !== null;
}

function applyOptionsByType(type) {
	var obj = options[type];

	var ele = $("#" + type + "-counter-div");
	ele.toggleClass("hide", !obj.visible);
	ele.css({ left: $.isNumeric(obj.x) ? parseInt(obj.x) : "", top: $.isNumeric(obj.y) ? parseInt(obj.y) : "" });

	setFont(
		type + ".counter-title",
		obj.title_font_family,
		obj.title_custom_font_family,
		obj.title_font_size,
		obj.title_font_weight,
		obj.title_italic,
		obj.title_underline,
		obj.title_color,
		obj.title_outline_color,
		obj.title_outline_width
	);
	setFont(
		type + ".counter",
		obj.num_font_family,
		obj.num_custom_font_family,
		obj.num_font_size,
		obj.num_font_weight,
		obj.num_italic,
		obj.num_underline,
		obj.num_color,
		obj.num_outline_color,
		obj.num_outline_width
	);

	$(".icon." + type).css("font-size", (type !== "like" ? obj.num_font_size : obj.title_font_size) + "px");

	// likes counters
	setFont(
		"like-counter-icon",
		"Material Icons",
		"",
		obj.title_font_size,
		obj.title_font_weight,
		obj.title_italic,
		obj.title_underline,
		obj.title_color,
		obj.title_outline_color,
		obj.title_outline_width
	);

	if (obj.title !== null) setText($("#" + type + "-counter-title"), obj.title);
}

function applyOptions() {
	applyOptionsByType("viewer");
	applyOptionsByType("viewer_i");
	applyOptionsByType("follower");
	applyOptionsByType("follower_i");
	applyOptionsByType("like");
}

function loadJS(filename, id) {
	var script = document.getElementById(id);
	if (script !== null) document.body.removeChild(script);

	script = document.createElement("script");
	script.type = "text/javascript";
	script.src = filename;
	script.id = id;
	script.defer = true;
	document.body.appendChild(script);
}

window.onload = function() {
	var channels = 0;
	for (var i = 0; i < platform_names.length; ++i) {
		if (getChannelId(platform_names[i])) ++channels;

		window[platform_names[i]].init();
	}

	getOptions();

	if (channels) {
		updateCounter();
		setInterval(function() {
			updateCounter();
		}, 60000);
	} else loadJS("js/edit.js", "edit-js");

	svg_bbox_interval = setInterval(function() {
		resizeAllSvg();
		if (svg_bbox_interval_count > 0) {
			if (++svg_bbox_interval_count > 30) clearInterval(svg_bbox_interval);
		}
	}, 2000);
	resizeAllSvg();
};
