//const key = "AIzaSyAZUSJ3iC887nejtt5cSePkCYvQA63YLqE";

const keys = ["AIzaSyB9-4ueznQ-grKEuKeiato0P2roGCRf69g"];

let key = keys[getRandomInt(0, keys.length - 1)];
//MrGuinas
var channelId = "UCCFcBOBr7B3ob4JjyTRdTRQ";
//Dublandos
//channelId = "UCP5JaLctW__URGWk6yx7YVg";

let props = {};

$(document).ready(function () {
  getLiveId();
});

function getLiveId() {
  return new Promise((resolve) => {
    console.log("YT Detecting livestream");
    var getVideoIdOptions =
      "https://www.googleapis.com/youtube/v3/search?part=snippet&eventType=live&type=video&maxResults=10&channelId=" +
      channelId +
      "&key=" +
      key;
    $.ajax({
      type: "GET",
      url: getVideoIdOptions,
      contentType: "application/json",
      cache: false,
      statusCode: {
        200: function (response) {
          console.log(response);
          if (response.items.length > 0) {
            console.log("YT Getting livestream properties");
            var liveChatId = response.items[0].id.videoId;
            check(liveChatId);
          } else {
            resolve(false);
          }
        },
        400: function (resultData) {
          video.liveChatId = null;
          video.videoId = null;
          console.log("Erro: " + resultData);
          resolve(false);
        },
        403: function (resultData) {
          console.log("Erro: " + resultData);
          key = keys[getRandomInt(0, keys.length - 1)];
          resolve(getLiveId());
        },
        404: function (resultData) {
          video.liveChatId = null;
          video.videoId = null;
          console.log("Erro: " + resultData);
          resolve(false);
        },
      },
    });
  });
}

function check(videoId) {
  console.log("YT Check");
  var getViewerOptions =
    "https://www.googleapis.com/youtube/v3/videos?part=liveStreamingDetails,statistics,snippet&id=" +
    videoId +
    "&key=" +
    key;
  $.ajax({
    type: "GET",
    url: getViewerOptions,
    contentType: "application/json",
    cache: false,
    success: function (response, textStatus, xhr) {
      console.log(response);
      addLiveStatus(response.items[0]);
      props = {
        viewers: response.items[0].liveStreamingDetails.concurrentViewers,
        likes: response.items[0].statistics.likeCount,
      };
      $("#viewers").html(`<h1>${props.viewers}</h1>`);
      $("#likes").html(`<h1>${props.likes}</h1>`);
      console.log(props);
    },
    complete: function (xhr, textStatus) {
      setTimeout(function () {
        check(videoId);
      }, 20000);
    },
  });
}
function addLiveStatus(ytData) {
  $.ajax({
    type: "POST",
    url: "https://multistreamer.xyz:21212/liveStatus",
    contentType: "application/json",
    dataType: "json",
    data: JSON.stringify(ytData),
    cache: false,
    statusCode: {
      200: function (response) {
        console.log(response);
      },
      404: function (resultData) {
        console.log(resultData);
      },
    },
  });
}
function getRandomInt(min, max) {
  //console.log(min,max);
  if (Math.ceil(min) == Math.floor(max)) {
    return min;
  } else {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
