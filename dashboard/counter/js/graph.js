google.charts.load('current', { 'packages': ['line'] });
var url_string = window.location.href;
var url = new URL(url_string);
var v1 = url.searchParams.get("v1");
var v2 = url.searchParams.get("v2")
$(document).ready(function() {
    getChannels();
    $('#channel1').on('change', function() {
        getVideos(1, this.value);
    });
    $('#channel2').on('change', function() {
        getVideos(2, this.value);
    });
    $('#video1').on('change', function() {
        if ($("#video1").val() != 0 && $("#video2").val() != 0 && $("#video1").val() != null && $("#video2").val() != null) {
            v1 = $("#video1").val();
            v2 = $("#video2").val();
            main();
        } else {}
    });
    $('#video2').on('change', function() {
        if ($("#video1").val() != 0 && $("#video2").val() != 0 && $("#video1").val() != null && $("#video2").val() != null) {
            v1 = $("#video1").val();
            v2 = $("#video2").val();
            main();
        } else {}
    });
    $('#generate').click(function() {
        if ($("#video1").val() != null && $("#video2").val() != null) {
            v1 = $("#video1").val();
            v2 = $("#video2").val();
            main();
        }
    });
});
var rows = [];
var channel = "";
var title = [];
var channelTitle = [];
var totViewers = [];
var count = [];
var totViews = [];
var subs = [];
var initSubs = [];
var likes = [];
var length = [];
var date = [];


async function main() {
    // var array2 = await check(1, 'UCTnkJfDle4yyOKsfjVEr2jg');
    // var array1 = await check(2, 'UCCFcBOBr7B3ob4JjyTRdTRQ');
    rows = [];
    var array1 = await check(1, v1);
    var array2 = await check(2, v2);
    var tam = 0;
    if (array1.length >= array2.length) {
        tam = array1.length;
    } else {
        tam = array2.length;
    }
    console.log(array1);
    console.log(array2);
    var last1 = 0;
    var last2 = 0;
    // for (let index = 0; index < tam; index++) {
    //     var element1 = array1[index];
    //     var element2 = array2[index];
    //     if (!element1) {
    //         element1 = 0;
    //     }
    //     if (!element2) {
    //         element2 = 0;
    //     }
    //     // rows.push([index, element1, element2]);
    //     rows.push([element1[0], element1[1], 0]);
    // }
    //console.log(array2);
    var temp = [];
    for (let index = 0; index < array1.length; index++) {
        var element1 = array1[index];
        temp.push({ "min": element1[0], "e1": element1[1], "e2": 0 });
    }
    for (let index = 0; index < array2.length; index++) {
        var element2 = array2[index];
        temp.push({ "min": element2[0], "e1": 0, "e2": element2[1] });
    }
    var last1 = null;
    var last2 = null;
    // console.log(temp);
    temp = _.orderBy(temp, ['min'], ['asc']);
    // console.log(temp);
    var index1 = 0;
    var index2 = 0;
    for (let index = 0; index < temp.length; index++) {
        var row = temp[index];

        if (row.e2 == 0) {
            if (index2 < array2.length) {
                rows.push([Math.round(row.min), row.e1, last2]);
            } else {
                rows.push([Math.round(row.min), row.e1, null]);
            }
            ++index1;
            last1 = row.e1;

        } else if (row.e1 == 0) {
            if (index1 < array1.length) {
                rows.push([Math.round(row.min), last1, row.e2]);
            } else {
                rows.push([Math.round(row.min), null, row.e2]);
            }
            ++index2;
            last2 = row.e2;
        }
    }
    // console.log(rows);
    google.charts.setOnLoadCallback(drawChart);
}

var chart = null;

function drawChart() {

    $("#tableStats").hide();
    $("#linechart_material").hide();
    $("#think").show();

    var data = new google.visualization.DataTable();
    data.addColumn('number', 'Minutos');
    data.addColumn('number', title[1] + " (" + channelTitle[1] + ")");
    data.addColumn('number', title[2] + " (" + channelTitle[2] + ")");
    data.addRows(rows);

    // data.addRows(4);
    // data.setCell(0, 0, 10);
    // data.setCell(0, 1, 50);

    // data.setCell(1, 0, 25);
    // data.setCell(1, 2, 180);

    // data.setCell(2, 0, 30);
    // data.setCell(2, 1, 120);
    // data.setCell(2, 2, 150);

    // data.setCell(3, 0, 38);
    // data.setCell(3, 2, 180);



    $("#channelTitle1").text(channelTitle[1]);
    $("#channelTitle2").text(channelTitle[2]);
    $("#totViews1").text(totViews[1] + " (" + (totViews[1] * 100 / subs[1]).toFixed(2) + ")");
    $("#totViews2").text(totViews[2] + " (" + (totViews[2] * 100 / subs[2]).toFixed(2) + ")");
    avg = [];
    avg[1] = Math.round(totViewers[1] / count[1]);
    avg[2] = Math.round(totViewers[2] / count[2]);
    $("#avgViewers1").text(avg[1] + " (" + (avg[1] * 100 / subs[1]).toFixed(2) + ")");
    $("#avgViewers2").text(avg[2] + " (" + (avg[2] * 100 / subs[2]).toFixed(2) + ")");
    varSubs = [];
    varSubs[1] = subs[1] - initSubs[1];
    varSubs[2] = subs[2] - initSubs[2];
    $("#subs1").text(subs[1] + " (" + (varSubs[1] > 0 ? "+" : "") + varSubs[1] + ")");
    $("#subs2").text(subs[2] + " (" + (varSubs[2] > 0 ? "+" : "") + varSubs[2] + ")");
    $("#likes1").text(likes[1]);
    $("#likes2").text(likes[2]);
    $("#length1").text(length[1]);
    $("#length2").text(length[2]);
    $("#date1").text(date[1]);
    $("#date2").text(date[2]);
    var chartwidth = $('#chartParent').width();
    var options = {
        width: chartwidth,
        chartArea: { width: chartwidth, left: 20, top: 20 },
        chart: {
            title: channelTitle[1] + " vs " + channelTitle[2],
            //subtitle: channelTitle
        },
        height: 500,
        legend: {
            position: 'none'
        },
    };

    chart = new google.charts.Line(document.getElementById('linechart_material'));

    chart.draw(data, google.charts.Line.convertOptions(options));
    $("#think").hide();
    $("#linechart_material").show();
    $("#tableStats").show();
}

function check(id, videoId) {
    return new Promise(resolve => {
        $.ajax({
            type: 'GET',
            url: "http://dashboard.mrguinas.com.br/counter/listYT.php?v=" + videoId,
            contentType: "application/json",
            cache: false,
            statusCode: {
                200: function(response) {
                    var i = 1;
                    var result = [];
                    totViewers[id] = 0;
                    count[id] = 0;
                    initSubs[id] = 0;
                    response.forEach(stats => {
                        stats = JSON.parse(stats);
                        //console.log(stats);
                        title[id] = stats.videoOptions.title;
                        channelTitle[id] = stats.videoOptions.channelTitle;
                        totViews[id] = stats.viewerOptions.statistics.viewCount;
                        if (stats.viewerOptions.liveStreamingDetails.concurrentViewers) {
                            totViewers[id] += parseInt(stats.viewerOptions.liveStreamingDetails.concurrentViewers);
                            ++count[id];
                        }
                        subs[id] = stats.followerOptions.statistics.subscriberCount;
                        likes[id] = stats.viewerOptions.statistics.likeCount + " / " + stats.viewerOptions.statistics.dislikeCount;
                        //console.log(moment(stats.viewerOptions.liveStreamingDetails.actualStartTime).format());
                        var dateStart = moment(stats.viewerOptions.liveStreamingDetails.actualStartTime);
                        var dateActual = moment(stats.timestamp * 1000);
                        length[id] = moment.duration(dateActual.diff(dateStart)).humanize()
                            //console.log(moment.duration(dateActual.diff(dateStart)).humanize());
                            //console.log(moment(stats.timestamp * 1000).format());
                        date[id] = moment(stats.viewerOptions.liveStreamingDetails.actualStartTime).format("DD/MM/YYYY - HH:mm");
                        if (initSubs[id] == 0) {
                            initSubs[id] = stats.followerOptions.statistics.subscriberCount;
                        }
                        result.push([moment.duration(dateActual.diff(dateStart)).asMinutes(), parseInt(stats.viewerOptions.liveStreamingDetails.concurrentViewers)]);
                        //console.log(moment.duration(dateActual.diff(dateStart)).asMinutes(), parseInt(stats.viewerOptions.liveStreamingDetails.concurrentViewers));
                    });
                    resolve(result);

                },
                404: function(resultData) {}
            }
        });
    });
}

function getChannels() {
    return new Promise(resolve => {
        $.ajax({
            type: 'GET',
            url: "http://dashboard.mrguinas.com.br/counter/listYTChannel.php",
            contentType: "application/json",
            cache: false,
            statusCode: {
                200: function(response) {
                    var i = 1;
                    var result = [];
                    var responseOrder = {};
                    Object.keys(response).sort().forEach(function(key) {
                        responseOrder[key] = response[key];
                    });
                    var channelIdArr = Object.values(responseOrder);
                    var channelTitleArr = Object.keys(responseOrder);
                    for (let index = 0; index < channelIdArr.length; index++) {
                        const channelId = channelIdArr[index];
                        const channelTitle = channelTitleArr[index];
                        $('#channel1').append($('<option>', {
                            value: channelId,
                            text: channelTitle
                        }));
                        $('#channel2').append($('<option>', {
                            value: channelId,
                            text: channelTitle
                        }));

                    }
                    resolve(result);

                },
                404: function(resultData) {}
            }
        });
    });
}

function getVideos(id, channelId) {
    return new Promise(resolve => {
        $.ajax({
            type: 'GET',
            url: "http://dashboard.mrguinas.com.br/counter/listYTVideos.php?c=" + channelId,
            contentType: "application/json",
            cache: false,
            statusCode: {
                200: function(response) {
                    var i = 1;
                    var result = [];
                    var videoTitleArr = Object.values(response);
                    var videoIdArr = Object.keys(response);
                    $('#video' + id).empty();
                    for (let index = 0; index < videoIdArr.length; index++) {
                        const videoTitle = videoTitleArr[index];
                        const videoId = videoIdArr[index];
                        $('#video' + id).append($('<option>', {
                            value: videoId,
                            text: videoTitle
                        }));
                    }
                    if ($("#video1").val() != 0 && $("#video2").val() != 0 && $("#video1").val() != null && $("#video2").val() != null) {
                        v1 = $("#video1").val();
                        v2 = $("#video2").val();
                        main();
                    } else {}
                    resolve(result);

                },
                404: function(resultData) {}
            }
        });
    });
}