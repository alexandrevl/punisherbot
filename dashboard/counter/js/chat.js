const key = 'AIzaSyDMIgGgUIQtpl-MwYEPI0usf4OiK0kQNYU'; 
//MrGuinas
const channelId = 'UCCFcBOBr7B3ob4JjyTRdTRQ';
//const channelId = 'UCXgs_fVuGdFYhDaCVrNp3KQ';
var video = {
    liveChatId: null,
    videoId: null
}
var isFirst = true;
var lastEtag = null;
var qntMessages = 0;
var poll = [];
const obs = new OBSWebSocket();

$(document).ready(function() {
    $('#login').click(function() {
        gapi.auth.authorize({
            client_id: OAUTH2_CLIENT_ID,
            scope: OAUTH2_SCOPES,
            immediate: false,
            cookie_policy: 'single_host_origin'
        }, handleAuthResult);
    });
    $('#bot').text("Connecting...");
    //initClient();
    //checkAuth();
});

var actions = [{
        "word": [
            "1",
            "2"
        ],
        "action": "poll",
        "param": "",
    },
    {
        "word": "ban",
        "action": "scene",
        "param": "Scene 3",
    },
    {
        "word": "xixi",
        "action": "scene",
        "param": "Scene 2",
    },
    {
        "word": "guinas",
        "action": "scene",
        "param": "mrguinas",
    },
    {
        "word": [
            "butico",
            "guinas"
        ],
        "action": "text",
        "param": "ban",
    }
];

async function main() {
    var isLive = await getLiveChatId();
    gapi.client.youtube.channels.list({
        "part": "snippet",
        "mine": true
    }).then(function(response) {
            // Handle the results here (response.result has the parsed body).
            //console.log("Response", response.result);
            //$('#bot').html('<img src="' + response.result.items[0].snippet.thumbnails.default.url + '" width="10%"> Bot: ' + response.result.items[0].snippet.title.trim());
            $('#bot').html('Bot: ' + response.result.items[0].snippet.title.trim());
            $('#login').text("Change");
            $('#login').show();
        },
        function(err) { console.error("Execute error", err); });
    gapi.client.youtube.channels.list({
        "part": "snippet",
        "id": channelId
    }).then(function(response) {
            // Handle the results here (response.result has the parsed body).
            // console.log("Response", response.result);
            $('#channelId').text("Channel: " + response.result.items[0].snippet.title);
        },
        function(err) { console.error("Execute error", err); });
    if (isLive) {
        console.log("Live: On");
        $('#liveStatus').text("Live: On");
        const address = "127.0.0.1:4444"
        console.log("Connecting OBS websocket: " + address);
        obs.connect({ address: address }, (err, data) => {
            if (err) {
                console.log("OBS websocket: not connected");
                $('#obs').text("Obs: Off");
            } else {
                console.log("OBS websocket: connected");
                $('#obs').text("Obs: On");
            }
        });
        getMessages(null);

    } else {
        //$('#bot').text("");
        $('#liveStatus').text("Live: Off");
        console.log("Live: Off");
    }
}

function getLiveChatId() {
    return new Promise(resolve => {
        console.log("Detecting livestream");
        if (video.liveChatId == null) {
            var getVideoIdOptions = 'https://www.googleapis.com/youtube/v3/search?part=snippet&eventType=live&type=video&maxResults=10&channelId=' + channelId + '&key=' + key;
            $.ajax({
                type: 'GET',
                url: getVideoIdOptions,
                contentType: "application/json",
                cache: false,
                statusCode: {
                    200: function(response) {
                        //console.log(response);
                        if (response.items.length > 0) {
                            console.log("Getting livestream properties");
                            video.videoId = response.items[0].id.videoId;
                            var getViewerOptions = 'https://www.googleapis.com/youtube/v3/videos?part=liveStreamingDetails,statistics&id=' + video.videoId + '&key=' + key;
                            $.ajax({
                                type: 'GET',
                                url: getViewerOptions,
                                contentType: "application/json",
                                cache: false,
                                statusCode: {
                                    200: function(response) {
                                        // console.log(response);
                                        video.liveChatId = response.items[0].liveStreamingDetails.activeLiveChatId;
                                        $('#liveChatId').text(video.liveChatId);
                                        // console.log(video);
                                        resolve(true);
                                    },
                                    400: function(resultData) {
                                        video.liveChatId = null;
                                        video.videoId = null;
                                        console.log("Erro", resultData);
                                        resolve(false);
                                    },
                                    403: function(resultData) {
                                        video.liveChatId = null;
                                        video.videoId = null;
                                        console.log("Erro", resultData);
                                        resolve(false);
                                    },
                                    404: function(resultData) {
                                        video.liveChatId = null;
                                        video.videoId = null;
                                        console.log("Erro", resultData);
                                        resolve(false);
                                    }
                                }
                            });
                        } else {
                            resolve(false);
                        }
                    },
                    400: function(resultData) {
                        video.liveChatId = null;
                        video.videoId = null;
                        console.log("Erro", resultData);
                        resolve(false);
                    },
                    404: function(resultData) {
                        video.liveChatId = null;
                        video.videoId = null;
                        console.log("Erro", resultData);
                        resolve(false);
                    }
                }
            });
        } else {
            resolve(true);
        }
    });

}


function getMessages(nextPageToken) {
    return new Promise(resolve => {
        var listMessages = null;
        if (!nextPageToken) {
            listMessages = 'https://www.googleapis.com/youtube/v3/liveChat/messages?liveChatId=' + video.liveChatId + '&part=snippet,authorDetails&maxResults=2000&key=' + key;
        } else {
            listMessages = 'https://www.googleapis.com/youtube/v3/liveChat/messages?liveChatId=' + video.liveChatId + '&part=snippet,authorDetails&pageToken=' + nextPageToken + '&key=' + key;
        }
        $.ajax({
            type: 'GET',
            url: listMessages,
            contentType: "application/json",
            cache: false,
            statusCode: {
                200: function(response) {
                    //console.log(response);
                    var show = false;
                    response.items.forEach(chatMessage => {
                        //console.log(chatMessage);
                        ++qntMessages;
                        var msg = chatMessage.snippet.displayMessage;
                        var author = chatMessage.authorDetails;
                        var publishedAt = chatMessage.snippet.publishedAt;
                        var childs = 0;
                        if ($("#chat").children().length >= 20) {
                            $("#chat").children()[0].remove();
                        }
                        $("#chat").append('<div><b>' + moment(publishedAt).format("HH:mm:ss") + ' ' + author.displayName + '</b>: ' + msg + '</div>');
                        if (!isFirst) {
                            if (chatMessage.snippet.superChatDetails) {
                                //sendMessage("❤ ❤ ❤ ❤ ❤ ❤ ❤ ❤ ❤ ❤ ❤ ❤ ");
                            }
                            actions.forEach(action => {
                                if (Array.isArray(action.word)) {
                                    action.word.forEach((word, i) => {
                                        if (msg.toLowerCase().includes(word)) {
                                            if (action.action == "text") {
                                                // sendMessage(action.param);
                                                //console.log(action.param);
                                            } else if (action.action == "poll") {
                                                if (poll[i] == null) {
                                                    poll[i] = 0;
                                                }
                                                poll[i]++;
                                                console.log(poll);
                                            }
                                        }
                                    });
                                } else {
                                    if (msg.toLowerCase().includes(action.word)) {
                                        if (action.action == "text") {
                                            sendMessage(action.param);
                                        } else if (action.action == "scene") {
                                            obs.setCurrentScene({
                                                'scene-name': action.param
                                            }, (err, data) => {
                                                if (err) {
                                                    console.log("Erro >>", err.description);
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                        lastEtag = chatMessage.etag;
                        //     }
                        // }
                    });
                    if (!show) {
                        lastEtag = null;
                    }
                    isFirst = false;
                    $('#qntMessages').text("Msg: " + qntMessages);
                    $('#poolingTime').text("Pool: " + parseInt(response.pollingIntervalMillis));
                    $('#totalResults').text("Total: " + parseInt(response.pageInfo.totalResults));
                    $('#resultsPerPage').text("PerPage: " + parseInt(response.pageInfo.resultsPerPage));
                    setTimeout(function() {
                        getMessages(response.nextPageToken);
                    }, parseInt(response.pollingIntervalMillis));
                    resolve(response.pollingIntervalMillis);
                },
                400: function(resultData) {
                    video.liveChatId = null;
                    video.videoId = null;
                    console.log("Erro", resultData);
                    resolve(false);
                },
                403: function(resultData) {
                    video.liveChatId = null;
                    video.videoId = null;
                    console.log("Erro", resultData);
                    resolve(false);
                },
                404: function(resultData) {
                    video.liveChatId = null;
                    video.videoId = null;
                    console.log("Erro", resultData);
                    resolve(false);
                }
            }
        });
    });
}

// Call the Data API to retrieve the playlist ID that uniquely identifies the
// list of videos uploaded to the currently authenticated user's channel.
function handleAPILoaded() {
    // See https://developers.google.com/youtube/v3/docs/channels/list
    // var request = gapi.client.youtube.channels.list({
    //     mine: true,
    //     part: 'contentDetails'
    // });
    // request.execute(function(response) {
    //     playlistId = response.result.items[0].contentDetails.relatedPlaylists.uploads;
    //     console.log(playlistId);
    // });
    main();

}

function sendMessage(messageText) {
    var request = gapi.client.youtube.liveChatMessages.insert({
        "part": "snippet",
        "snippet": {
            "liveChatId": video.liveChatId,
            "type": "textMessageEvent",
            "textMessageDetails": {
                "messageText": messageText
            }
        }
    });
    request.execute(function(response) {
        console.log(response);
    });
}