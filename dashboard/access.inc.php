<?php
ini_set('session.gc_maxlifetime', 3600*10);
session_set_cookie_params(3600*10);
session_start();
if ($_SESSION['access'] != true) { 
    header("Location: https://dashboard.mrguinas.com.br/login.php");
    die(); 
}
?>