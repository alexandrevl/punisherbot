$(document).ready(function() {
	setInterval(function() {
		check();
	}, 1000);
});

function check() {
	var jsonPost = {};
	$.ajax({
		type: "GET",
		url: "https://multistreamer.xyz:21212/meta",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				var percent = Math.round((response.apurado / response.meta) * 100);
				if (percent > 100) {
					percent == 100;
					response.apurado = response.meta;
				}
				$("#barP").attr("aria-valuenow", "" + percent + "");
				$("#barP").attr("style", "width: " + percent + "%");
				$("#textMeta").text(response.text);
				$("#apuradoText").text("R$ " + parseFloat(response.apurado).toFixed(2));
				$("#metaText").text("R$ " + parseFloat(response.meta).toFixed(2));
			},
			404: function(resultData) {}
		}
	});
}
