$(document).ready(function() {
	setInterval(function() {
		// check();
	}, 1000);
	check();
	$("#slideshow > div:gt(0)").hide();
});

function check() {
	//console.log("Check...");
	$.ajax({
		type: "GET",
		url: "https://multistreamer.xyz:21212/userDonnation",
		contentType: "application/json",
		dataType: "json",
		data: "{ }",
		cache: false,
		statusCode: {
			200: function(response) {
				if (response.length > 0) {
					var itens = [];
					qntCol = 1;
					//console.log(response);
					unicos = _.uniqBy(response, "channelId");
					unicos.forEach((user) => {
						item =
							"<div style='color:white; display: none'><a href='https://www.youtube.com/channel/" +
							user.channelId +
							"' target='_blank'><img src='" +
							user.avatar +
							"' height=400 class='rounded-circle mx-auto d-block'></a><div class='text-center'>" +
							user.name +
							"</div></div>";
						//console.log(item);
						itens.push(item);
					});
					//console.log(itens);
					$("#slideshow").html(itens);
					updateDiv();
					setInterval(function() {
						updateDiv();
					}, 3000);
				}
			},
			404: function(resultData) {}
		}
	});
}

function updateDiv() {
	$("#slideshow > div:first")
		.fadeOut(1000)
		.next()
		.fadeIn(1000)
		.end()
		.appendTo("#slideshow");
}
