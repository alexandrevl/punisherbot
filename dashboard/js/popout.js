$(document).ready(function() {
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyBGHtVCceJzW-uQMQWNbRhjt3fSrUBxPr8",
        authDomain: "mrguinas-174619.firebaseapp.com",
        databaseURL: "https://mrguinas-174619.firebaseio.com",
        projectId: "mrguinas-174619",
        storageBucket: "mrguinas-174619.appspot.com",
        messagingSenderId: "719120864815"
    };
    firebase.initializeApp(config);
    var database = firebase.database();
    var refFirebase = database.ref('overlay/social');

    $("#twitch").click(function() {
        var data = {
            index: 5,
            text: "mr_guinas",
            seconds: 10,
            random: Math.random()
        }
        refFirebase.set(data);
    });
    $("#twitter").click(function() {
        var data = {
            index: 0,
            text: "@mr_guinas",
            seconds: 10,
            random: Math.random()
        }
        refFirebase.set(data);
    });
    $("#facebook").click(function() {
        var data = {
            index: 1,
            text: "/MrGuinas",
            seconds: 10,
            random: Math.random()
        }
        refFirebase.set(data);
    });
    $("#instagram").click(function() {
        var data = {
            index: 2,
            text: "@mr_guinas",
            seconds: 10,
            random: Math.random()
        }
        refFirebase.set(data);
    });
    $("#youtube").click(function() {
        var data = {
            index: 3,
            text: "Inscreva-se",
            seconds: 10,
            random: Math.random()
        }
        refFirebase.set(data);
    });
    $("#like").click(function() {
        var data = {
            index: 4,
            text: "Deixa o like!",
            seconds: 10,
            random: Math.random()
        }
        refFirebase.set(data);
    });

    $('#titleDialog').on('hidden.bs.modal', function(e) {
        document.getElementById('meta').contentWindow.location.reload();
    })



    $("#green").click(function() {
        $("#green").prop('disabled', true);
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://color.mrguinas.com.br/changeColor.php?c=2",
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
            $("#green").prop('enabled', true);
        });
    });
    $("#red").click(function() {
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://color.mrguinas.com.br/changeColor.php?c=0",
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    });
    $("#blue").click(function() {
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://color.mrguinas.com.br/changeColor.php?c=1",
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    });
    $("#orange").click(function() {
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://color.mrguinas.com.br/changeColor.php?c=3",
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    });
    $("#pink").click(function() {
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://color.mrguinas.com.br/changeColor.php?c=4",
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    });
    $("#purple").click(function() {
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://color.mrguinas.com.br/changeColor.php?c=5",
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    });
    $("#rainbow").click(function() {
        $.ajaxSetup({ cache: false });
        $.ajax({
            type: 'GET',
            url: "http://color.mrguinas.com.br/superchat.php",
            'uniq_param': (new Date()).getTime(),
            dataType: "text/plain"
        }).fail(function(data) {
            console.log(data);
        }).done(function(data) {
            console.log("Sucesso");
        });
    });
});