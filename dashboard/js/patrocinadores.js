let qntResp = 0;
// let width = 500;
function check() {
  var jsonPost = {};
  $.ajax({
    type: "GET",
    url: "https://multistreamer.xyz:21212/patrocinadores",
    contentType: "application/json",
    dataType: "json",
    cache: false,
    statusCode: {
      200: function(response) {
        console.log(response);
        let isChanged = false;
        let text = "PATROCINADO POR: ";
        if (response.length != qntResp) {
          isChanged = true;
        }
        if (isChanged) {
          if (response.length > 0) {
            response.forEach(patro => {
              text += `<img src="${patro.avatar}" height=80 class='rounded-circle'>${patro.name}, `;
            });
            // wt = width + response.length * 0  ;
            // $("#letreiro").html(
            //   `<p width="${wt}px">${text.substring(0, text.length - 2)}</p>`
            // );
            $("#letreiro").html(`${text.substring(0, text.length - 2)}`);
            $("#boxLetreiro").show();
          } else {
            $("#boxLetreiro").hide();
            $("#letreiro").html(``);
          }
        }

        qntResp = response.length;
      },
      404: function(resultData) {}
    }
  });
}
$(document).ready(function() {
  setInterval(function() {
    check();
  }, 5000);
});
check();
