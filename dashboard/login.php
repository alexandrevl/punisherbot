<html>
    <head>
    <title>
        The Punisher - Control Panel
    </title>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="icon" type="image/png" href="img/Logo.png" />
    <script src="js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
    <script src="js/login.js" type="text/javascript"></script>
    <script src="js/md5.js" type="text/javascript"></script>

    </head>
    <body>
    <br>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-4">
                    <div class="alert alert-danger" style="display: none" role="alert" id="alert">
                        Comandos
                    </div>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-4">
                    <div class="card">
                        <div class="card-header">
                            Login
                        </div>
                        <div class="card-block">
                            <div class="form-group row">
                                <label for="inputPassword3" class="col-3 col-form-label">Password</label><br>
                                <div class="col-6">
                                    <input type="password" class="form-control" id="inputPassword" placeholder="">
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary" id="signIn">Sign In</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>