<?php 
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    include 'Punishment.class.php';
    require_once 'vendor/autoload.php';
    
    $client = new Google_Client();
    $client->setApplicationName("YTapi");
    $client->setDeveloperKey("AIzaSyBC8H_T440Dy1tzkc57h23x3zQYy_gds4A");

    session_start();
    $db = "";

    if (!isset($_SESSION["db"])) {
        $db = new SQLite3('mysqlitedb.db');
    }

    $punishment = new Punishment();
    $punishment->punisher = "";
    $punishment->ts_ini = 0;
    $punishment->cod_pun = 0;
    $punishment->tx_pun = "";
    $punishment->source = 0;
    $punishment->ts_fim = 0;
    $punishment->channel_id = "";
    $punishment->thumb_url = "";

    $isPunished = false;

    $result = $db->query("select * from punishment where ts_fim > " . time(). " and ts_ini < " . time());
    $json = "";
    while ($row = $result->fetchArray()) {
        //$json = json_encode($row);
        $punishment->punisher = $row['user'];
        $punishment->ts_fim = $row['ts_fim'];
        $punishment->cod_pun = $row['cod_pun'];
        $punishment->tx_pun = $row['tx_pun'];
        $punishment->source = $row['source'];
        $punishment->ts_ini = $row['ts_ini'];
        $punishment->channel_id = $row['channel_id'];
        $isPunished = true;
    }
    if ($isPunished) {
        if ($punishment->channel_id != "") {
            
            $service = new Google_Service_YouTube($client);
            $optParams = array('id' => $punishment->channel_id);
            
            $results = $service->channels->listChannels("snippet", $optParams) ;
            
            foreach ($results as $item) {
                //echo $item['snippet']['title'], "<br /> \n";
                //echo '<img src="'. $item['snippet']['thumbnails']['default']['url'] . '"/> <br />';
                $punishment->thumb_url = $item['snippet']['thumbnails']['default']['url'];
            }
        }


        $json = json_encode($punishment, JSON_UNESCAPED_UNICODE);
        echo $json;
    } else {
        http_response_code(404);
    }
    die;
?>