<?php  
//include 'access.inc.php'; 
?>
<html>
    <head>
        <title>
            The Punisher - Control Panel
        </title>
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <link rel="icon" type="image/png" href="img/Logo.png" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/milestones.js" type="text/javascript"></script>
    </head>
    <body>
        <br>
        <div class="container">
        <div class="row">
        <div class="col-12">
            <table class="table" id="listPunishments">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Milestone</th>
                    <th>Remover</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>
        </div>
        </div>
        <div class="row">
        <div class="col">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Adicionar Milestone
                </button>
        </div>
        </div>
        <div class="row"><br></div>
        <div class="row">
            <div class="col-3"></div>
            <div class="col">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary" id="prev"><i class="fa fa-caret-up" aria-hidden="true"></i>&nbsp;Voltar</button>
                    <button type="button" class="btn btn-outline-danger" id="reset">Reset</button>
                    <button type="button" class="btn btn-outline-primary" id="next"><i class="fa fa-caret-down" aria-hidden="true"></i>&nbsp;Próximo</button>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col">
        <br>
            Link pra colocar no OBS: <a href="http://mrguinas.com.br/nb/popupMilestones.html" target="popupMilestones">http://mrguinas.com.br/nb/popupMilestones.html</a>
        </div>
        </div>
        <div class="row">
        <div class="col">
        <br>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Adicionar Milestone</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <label for="tx_pun">Texto do Milestone</label>
                            <input type="text" class="form-control" id="tx_milestone" placeholder="" maxlength="18">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="saveMilestone">Adicionar Milestone</button>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
    </body>
</html>
