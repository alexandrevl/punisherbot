<?php  
//include 'access.inc.php'; 
?>
<html>
    <head>
        <title>
            The Punisher - Control Panel
        </title>
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <link rel="icon" type="image/png" href="img/Logo.png" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/milestonesControl.js" type="text/javascript"></script>
    </head>
    <body>
        <br>
        <div class="container">
        <div class="row">
        <div class="col">
            <table class="table table-striped" id="listPunishments">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Milestone</th>
                    <th>Status</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>
            </div>
            </div>
        </div>
    </body>
</html>
