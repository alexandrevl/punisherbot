<?php  
include 'access.inc.php'; 
?>
<html>
    <head>
        <title>
            The Punisher - Control Panel
        </title>
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <link rel="icon" type="image/png" href="img/Logo.png" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/4088d9cd03.js"></script>
        <script src="js/config.js" type="text/javascript"></script>
    </head>
    <body>
        <br>
        <div class="container">
        <div class="row">
        <div class="col">
            <table class="table table-striped" id="listPunishments">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Texto punição</th>
                    <th>Tempo (s)</th>
                    <th>Força (% chance)</th>
                    <th>Remover</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>
            <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Adicionar Punição
                </button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Adicionar Punição</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group">
                            <label for="tx_pun">Texto da punição</label>
                            <input type="text" class="form-control" id="tx_pun" placeholder="" maxlength="20">
                        </div>
                        <div class="form-group">
                            <label for="tempo">Tempo (s)</label>
                            <select class="form-control" id="tempo">
                                <option>10</option>
                                <option>20</option>
                                <option>30</option>
                                <option>40</option>
                                <option>50</option>
                                <option>60</option>
                                <option>70</option>
                                <option>80</option>
                                <option>90</option>
                                <option>100</option>
                                <option>110</option>
                                <option>120</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="forca">Força</label>
                            <select class="form-control" id="forca">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="savePunishment">Adicionar punição</button>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
    </body>
</html>