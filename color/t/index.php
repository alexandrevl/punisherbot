<?php
    header('Content-Type: text/html; charset=utf-8'); 
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    include '../color.php';

    session_start();
    $db = ""; 
    $user = "default";
    $isJSON = false;
    //$min = rand(5,10);
    $min = 20;
    $tentativa = array();

    if(isset($_GET['u'])) {
        $user = $_GET['u'];
    }
    if(isset($_GET['j'])) {
        $isJSON = true;
    }
    if(isset($_POST['Nightbot-User'])) {
        $user = $_POST['Nightbot-User'];
    }

    if (!isset($_SESSION["db"])) {
        $db = new SQLite3('../color.db');
    }
    $tentativa['user'] = $user;
    $tentativa['isHit'] = false;
    $tentativa['minToAdd'] = $min;
    $tentativa['timestamp'] = time();
    $tentativa['nextTs'] = 0;
    $tentativa['diffTs'] = 0;

    $stmt = $db->prepare("INSERT INTO tentativas (user, timestamp) VALUES (:user, :ts)");
    $stmt->bindValue(':user', $tentativa['user'] , SQLITE3_TEXT);
    $stmt->bindValue(':ts', $tentativa['timestamp'], SQLITE3_INTEGER);
    $result = $stmt->execute(); 

    
    $result = $db->query("select max(timestamp) as ts from acertos");
    //echo $result;
    while ($row = $result->fetchArray()) {
        $ts = $row['ts'];
        //$minToAdd = $min*60;
        $minToAdd = $min;
        $nextTs = $ts+$minToAdd;
        $tentativa['nextTs'] = $nextTs;
        $tentativa['diffTs'] = $tentativa['nextTs'] - $tentativa['timestamp'];
        if ($tentativa['timestamp']>$nextTs) {
            $tentativa['isHit'] = true;
            if (!$isJSON) {
                $stmt = $db->prepare("INSERT INTO acertos (user, timestamp) VALUES (:user, :ts)");
                $stmt->bindValue(':user', $tentativa['user'] , SQLITE3_TEXT);
                $stmt->bindValue(':ts', $tentativa['timestamp'], SQLITE3_INTEGER);
                $result2 = $stmt->execute(); 
                $finalColor = colorRoulette(30);
                $tentativa['color'] = $finalColor['name'];
            }
        }
    }

    $json = json_encode($tentativa, JSON_UNESCAPED_UNICODE);
    if ($isJSON) {
        echo $json;
    } else {
        if ($tentativa['isHit']) {
            echo $tentativa['user'] . ', você conseguiu! Vamos mudar a cor do MrGuinas!!!!';
        } else {
            echo 'No rainbow now';
        }
    }
    die;

?>