<?php
$token = "";
//MrGuinas
$deviceId = "8012754D40A51DFD7DC639AEB17B346118AE7228";

//Sala
//$deviceId = "80120D52725F115812C58D0E677DA553187D9A56";

$token = getToken();
$actualColor = getColor($deviceId,$token);
$finalColor = colorRoulette($deviceId,25,$token);
changeColor($deviceId,$actualColor,$token);



function getColor($deviceId, $token) {

    $url = 'https://use1-wap.tplinkcloud.com/?token=' . $token ;

    $data = '
    {  
        "method":"passthrough",
        "params":{  
        "deviceId":"' . $deviceId . '",
        "requestData":"{\"system\":{\"get_sysinfo\":\"\"}}"
        }
    }
    ';

    $additional_headers = array(                                                                          
    'Content-Type: application/json'
    );

    $ch = curl_init($url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers); 

    $server_output = curl_exec ($ch);

    //echo $server_output;
    $json = json_decode($server_output, JSON_UNESCAPED_UNICODE);
    //print_r($json['result']); 
    $responseData = json_decode($json['result']['responseData'], JSON_UNESCAPED_UNICODE);
    //echo $json['result']['responseData'];
    //print_r($responseData['system']['get_sysinfo']['light_state']['color_temp']);
    //echo $responseData['system']['get_sysinfo']['light_state']['color_temp'];
    $actualColor = array(
        // "hue" =>  $responseData['system']['get_sysinfo']['light_state']['hue'],
        // "saturation" =>  $responseData['system']['get_sysinfo']['light_state']['saturation'],
        "brightness" =>  $responseData['system']['get_sysinfo']['light_state']['brightness'],
        // "color_temp" =>  $responseData['system']['get_sysinfo']['light_state']['color_temp'],
        "hue" =>  0,
        "saturation" =>  0,
        //"brightness" =>  100,
        "color_temp" =>  5000,
    );
    return $actualColor;
}


function colorRoulette($deviceId, $qnt, $token) {
    $color = array(
        array("name"=>"red","hue" => "0","saturation"=> "100","brightness"=> "80","color_temp"=> "0"),
        array("name"=>"blue","hue" => "240","saturation"=> "100","brightness"=> "80","color_temp"=> "0"),
        array("name"=>"green","hue" => "120","saturation"=> "100","brightness"=> "80","color_temp"=> "0"),
        array("name"=>"orange","hue" => "30","saturation"=> "100","brightness"=> "80","color_temp"=> "0"),
        array("name"=>"pink","hue" => "330","saturation"=> "100","brightness"=> "80","color_temp"=> "0"),
        array("name"=>"purple","hue" => "276","saturation"=> "100","brightness"=> "60","color_temp"=> "0")
    );
    for ($i=0; $i < $qnt; $i++) { 
        changeColor($deviceId,$color[rand(0,4)],$token);
    }
    $finalColor = $color[rand(0,4)];
    changeColor($deviceId,$finalColor,$token);
    return $finalColor;
}
function getToken() {
    $url = 'https://wap.tplinkcloud.com/';
    $data = '
    {
        "method": "login",
        "params": {
        "appType": "Kasa_Android",
        "cloudUserName": "alexandrevl@gmail.com",
        "cloudPassword": "12345678",
        "terminalUUID": "377c4b28-cfad-439e-b885-7a749baab03b"
        }
       }
    ';

    $additional_headers = array(                                                                          
    'Content-Type: application/json'
    );

    $ch = curl_init($url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers); 

    $server_output = curl_exec ($ch);
    $json = json_decode($server_output, JSON_UNESCAPED_UNICODE);
    $token =  $json['result']['token'];
    return $token;
}
function changeColor($deviceId,$color,$token) {

    $url = 'https://use1-wap.tplinkcloud.com/?token=' . $token ;

    $data = '
    {  
        "method":"passthrough",
        "params":{  
        "deviceId":"' . $deviceId . '",
        "requestData":"{\"smartlife.iot.smartbulb.lightingservice\":{\"transition_light_state\":{\"ignore_default\":1,\"transition_period\":75,\"mode\":\"normal\",\"hue\":' . $color["hue"] . ',\"on_off\":1,\"saturation\":' . $color["saturation"] . ',\"color_temp\":' . $color["color_temp"] . ',\"brightness\":' . $color["brightness"] . '}}}"
        }
    }
    ';

    $additional_headers = array(                                                                          
    'Content-Type: application/json'
    );

    $ch = curl_init($url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers); 

    $server_output = curl_exec ($ch);

    return $server_output;
}
?>